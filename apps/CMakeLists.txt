set(PROGRAMS dcedit dcmod dcinv)

include_directories(${GIMLI_INCLUDE_DIR})
include_directories(${GIMLI_INCLUDE_DIR}/bert)
include_directories(${PROJECT_BINARY_DIR})

add_definitions(-std=c++0x)

foreach(program ${PROGRAMS})
    add_executable(${program} ${program}.cpp)
    target_link_libraries(${program} ${GIMLI_LIBRARIES})
    if (Boost_THREAD_FOUND)
        target_link_libraries(${program} ${Boost_THREAD_LIBRARY} ${Boost_SYSTEM_LIBRARY})
        target_link_libraries(${program} ${Boost_SYSTEM_LIBRARY})
    endif (Boost_THREAD_FOUND)
    install(TARGETS ${program} DESTINATION bin)
endforeach(program)

set(BERT_SCRIPTS bert; bertNew2D; bertNew3D; bertNew2DTopo; bertNew3DTopo bertopts.cfg)

foreach(program ${BERT_SCRIPTS})
    file(COPY ${program} DESTINATION ${CMAKE_BINARY_DIR}/bin)
    install(PROGRAMS ${program} DESTINATION bin)
endforeach(program)

install(FILES bertopts.cfg DESTINATION bin)
