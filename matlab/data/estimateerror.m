function [err,out]=estimateerror(Data,proz,Umin,I)

% ESTIMATEERROR - Estimate Errors (and noisyfies data)
% error = estimateerror(Data,proz,Umin,I)
% [Data.err,noisy_data] = estimateerror(Data,proz,Umin,I)
% Data - data structure with data(rhoa/R/U+I) and geometric factors(Data.k)
% proz - relative error in percent (1%) - scalar or vector 
% Umin - voltage error (100�V)
% I - driving current if not given in file (100mA)

if nargin<1, error('Data set required'); end
if nargin<2, proz=3; end
if nargin<3, Umin=100e-6; end
if nargin<4, I=100e-3; end

if isfield( Data, 'i' )&&( length( Data.i) == length( Data.k ) ), 
    I=Data.i; 
end
if isfield( Data, 'u' )&&( length( Data.u) == length( Data.k ) ),
    err = abs( Umin./ Data.u ) + proz/100;
else
    if isfield(Data,'rho')&&(length(Data.rho)==length(Data.a)),
        err = abs( 1./ Data.rho ) * Umin./I + proz/100;
    elseif isfield(Data,'r')&&(length(Data.r)==length(Data.a)),
        err = abs( Data.k./Data.r)*Umin./I+proz/100;
    else
        display('Error estimation: Could not get voltage information!');
        err=ones(size(Data.a))*proz/100;
    end
end
if nargout>1,
  noise=randn(size(Data.a)).*err;  
  if isfield(Data,'r'), out=Data.r.*(1+noise); end
  message(sprintf('Error min=%.1f%% max=%.1f%% mean=%.1f%%',...
      min(abs(noise))*100,max(abs(noise))*100,mean(abs(noise))*100));
end
