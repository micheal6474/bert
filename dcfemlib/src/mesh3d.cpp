// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "mesh3d.h"
#include "refine.h"

#include "setalgorithm.h"

namespace MyMesh{

Mesh3D::Mesh3D( const Mesh3D & mesh ) : BaseMesh( mesh ) {;
  for ( int i = 0; i < mesh.cellCount(); i++) BaseMesh::createCell( mesh.cell(i) );
  for ( int i = 0; i < mesh.boundaryCount(); i++) BaseMesh::createBoundary( mesh.boundary( i ) );
}

Mesh3D::Mesh3D( const string & fname ) : BaseMesh( ) {
  BaseMesh::load( fname );
}

Mesh3D::~Mesh3D(){;
 clear();
}

BaseElement * Mesh3D::createTetrahedron_( Node & a, Node & b, Node & c, Node & d, int id, double attribute ){
  if ( id == -1 ) id = pCellVector_.size();
  BaseElement * e = new Tetrahedron( a, b, c, d, id, attribute );

  pCellVector_.push_back( e );
  for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertCell( e );
  return e;
}

BaseElement * Mesh3D::createTetrahedron10_( vector < Node * > & pNodeVector, int id, double attribute ){
  if ( id == -1 ) id = pCellVector_.size();
  BaseElement * e = new Tetrahedron10( *pNodeVector[ 0 ], *pNodeVector[ 1 ], *pNodeVector[ 2 ], *pNodeVector[ 3 ], id, attribute );
  for ( int i = 4; i < 10; i ++ ) e->setNode( i, *pNodeVector[ i ] );

  pCellVector_.push_back( e );
  for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertCell( e );
  return e;
}

BaseElement * Mesh3D::createHexahedron_( Node & n1, Node & n2, Node & n3, Node & n4,
					 Node & n5, Node & n6, Node & n7, Node & n8, int id, double attribute ){
  if ( id == -1 ) id = pCellVector_.size();
  BaseElement * e = new Hexahedron( n1, n2, n3, n4, n5, n6, n7, n8, id, attribute );

  pCellVector_.push_back( e );
  for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertCell( e );
  return e;
}

BaseElement * Mesh3D::createTriangleFace_( Node & a, Node & b, Node & c, int id, int marker ){
  BaseElement * e = findBoundary( a, b, c );

  if ( e == NULL ) {
    if ( id == -1 ) id = pBoundaryVector_.size();
    e = new TriangleFace( a, b, c, id, marker );
    pBoundaryVector_.push_back( e );
    e->setId( id );
    e->setMarker( marker );
    for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertBoundary( e );
  } else {
    if ( marker > 0 ) e->setMarker( marker );
  }

  return e;
}

BaseElement * Mesh3D::createTriangle6Face_( vector < Node * > & pNodeVector, int id, int marker ){
  BaseElement * e = findBoundary( *pNodeVector[ 0 ], *pNodeVector[ 1 ], *pNodeVector[ 2 ] );

  if ( !e ){
        if ( id == -1 ) id = pBoundaryVector_.size();
        e = new Triangle6Face( *pNodeVector[ 0 ], *pNodeVector[ 1 ], *pNodeVector[ 2 ], id, marker );
        for ( int i = 3; i < 6; i ++ ) e->setNode( i, *pNodeVector[ i ] );
        pBoundaryVector_.push_back( e );
        for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertBoundary( e );
  }
  return e;
}

BaseElement * Mesh3D::createQuadrangleFace_( Node & a, Node & b, Node & c, Node & d, int id, int marker ){
  if ( id == -1 ) id = pBoundaryVector_.size();
  BaseElement * e = new QuadrangleFace( a, b, c, d, id, marker );
  pBoundaryVector_.push_back( e );
  for ( int i = 0; i < e->nodeCount(); i ++ ) e->node( i ).insertBoundary( e );
  return e;
}

BaseElement * Mesh3D::createTetrahedron( vector < Node * > & pNodeVector, int id, double attribute ){
  BaseElement * e = NULL;
  switch ( pNodeVector.size() ) {
  case 4:
    e = createTetrahedron_( *pNodeVector[ 0 ], *pNodeVector[ 1 ], *pNodeVector[ 2 ], *pNodeVector[ 3 ], id, attribute );
    break;
  case 10:
    e = createTetrahedron10_( pNodeVector, id, attribute );
    dynamic_cast< Tetrahedron10 * >(e)->check();
    break;
  }
  return e;
}

BaseElement * Mesh3D::createTriangleFace( vector < Node *> & pNodeVector, int id, int marker ){
  BaseElement * t = NULL;
  switch ( pNodeVector.size() ){
  case 3:
    t = createTriangleFace_( *pNodeVector[ 0 ], *pNodeVector[ 1 ], *pNodeVector[ 2 ], id, marker );
    break;
  case 6:
    t = createTriangle6Face_( pNodeVector, id, marker );
    dynamic_cast< Triangle6Face * >(t)->check();
    break;
  default: cerr << WHERE_AM_I << " format not known " << pNodeVector.size() << endl;
  }
  return t;
}

BaseElement * Mesh3D::createCellFromNodeIdxVector( const vector < long > & idx, double attribute ){
  vector < Node * > pNodeVector;

  switch( idx.size() ){
  case 4: //** 4-node tetrahedron
    return createTetrahedron_( node( idx[ 0 ] ), node( idx[ 1 ] ), node( idx[ 2 ] ), node( idx[ 3 ] ),
			       cellCount(), attribute );
    break;
  case 10: //** 10 node tetrahedron
    pNodeVector.clear();
    for ( int i = 0; i < 10; i ++ ) pNodeVector.push_back( &node( idx[ i ] ) );
    return createTetrahedron10_( pNodeVector, cellCount(), attribute );
    break;
  default:
    cerr << WHERE_AM_I << " cell type must be defined " << idx.size() << endl; exit( 1 );
  }
  return NULL;
}

BaseElement * Mesh3D::createBoundaryFromNodeIdxVector( const vector < long > & idx, int marker ){
  vector < Node * > pNodeVector;
  switch( idx.size() ){
  case 3:
    return createTriangleFace_( node( idx[ 0 ] ), node( idx[ 1 ] ), node( idx[ 2 ] ), boundaryCount(), marker ); break;
  case 6: // 6 node triangle
    for ( int i = 0; i < 6; i ++ ) pNodeVector.push_back( &node( idx[ i ] ) );
    return createTriangle6Face_( pNodeVector, boundaryCount(), marker ); break;
  default:
    cerr << WHERE_AM_I << " boundary type must be defined " << idx.size() << endl; exit( 1 );
  }
  return NULL;
}

void Mesh3D::createNeighoursInfos( ){
  BaseElement *face = NULL;
  for ( int i = 0; i < cellCount(); i ++ ){
    BaseElement *e = &cell( i );

    face = createTriangleFace( e->node( 0 ), e->node( 1 ), e->node( 2 ) );
    if ( face->leftCell() == NULL ) face->setLeftCell( *e ); else face->setRightCell( *e );

    face = createTriangleFace( e->node( 0 ), e->node( 1 ), e->node( 3 ) );
    if ( face->leftCell() == NULL ) face->setLeftCell( *e ); else face->setRightCell( *e );

    face = createTriangleFace( e->node( 1 ), e->node( 2 ), e->node( 3 ) );
    if ( face->leftCell() == NULL ) face->setLeftCell( *e ); else face->setRightCell( *e );

    face = createTriangleFace( e->node( 2 ), e->node( 0 ), e->node( 3 ) );
    if ( face->leftCell() == NULL ) face->setLeftCell( *e ); else face->setRightCell( *e );
  }
}

BaseElement * Mesh3D::findCellAt( RealPos & pos ){
  return NULL;
}

BaseElement * Mesh3D::findCellAt( TriangleFace & face ){
  return face.findBoundaryCell();
}

BaseElement * Mesh3D::findBoundary( Node & n0, Node & n1, Node & n2){

//   cout << "node0 " << n0.id() <<  endl;
//   show( n0.faceSet() );
//   cout << "node1 " << n1.id() <<  endl;
//   show( n1.faceSet() );
//   cout << "node2 " << n2.id() <<  endl;
//   show( n2.faceSet() );

  SetpBounds commonFaces( n0.boundarySet() & n1.boundarySet() & n2.boundarySet() );

  //  show( commonFaces );
  if ( commonFaces.size() == 1 ) return (*commonFaces.begin());
  return NULL;
}

BaseElement * Mesh3D::findBoundary( BaseElement & e1, BaseElement & e2 ) {

  SetpCells facesE1;
  facesE1.insert( findBoundary( e1.node(0), e1.node(1), e1.node(2) ) );
  facesE1.insert( findBoundary( e1.node(0), e1.node(1), e1.node(3) ) );
  facesE1.insert( findBoundary( e1.node(1), e1.node(2), e1.node(3) ) );
  facesE1.insert( findBoundary( e1.node(2), e1.node(0), e1.node(3) ) );

  SetpCells facesE2;
  facesE2.insert( findBoundary( e2.node(0), e2.node(1), e2.node(2) ) );
  facesE2.insert( findBoundary( e2.node(0), e2.node(1), e2.node(3) ) );
  facesE2.insert( findBoundary( e2.node(1), e2.node(2), e2.node(3) ) );
  facesE2.insert( findBoundary( e2.node(2), e2.node(0), e2.node(3) ) );

  //  show( facesE1 );
  //show( facesE2 );

  SetpCells commonFaces( facesE1 & facesE2 );

  //  cout << "common" << endl;
  //show( commonFaces );

  if ( commonFaces.size() == 1 ) return (*commonFaces.begin());
  return NULL;
}

// void Mesh3D::createP2MeshNew( const BaseMesh & mesh ){
//   clear();

//   for ( int i = 0, imax = mesh.nodeCount(); i < imax; i ++ ) this->createNode( mesh.node( i ) );

//   SparcepNodeMatrix nodeMatrix( nodeCount() );
//   vector < Node * > pNodeVector;

//   Node *n[ 10 ];

//   TriangleFace * face = NULL;

//   for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
//     pNodeVector.clear();
//     n[ 0 ] = &node( mesh.cell( i ).node( 0 ).id() );
//     n[ 1 ] = &node( mesh.cell( i ).node( 1 ).id() );
//     n[ 2 ] = &node( mesh.cell( i ).node( 2 ).id() );
//     n[ 3 ] = &node( mesh.cell( i ).node( 3 ).id() );

//     for ( int j = 0; j < 4; j ++ ) pNodeVector.push_back( n[ j ] );

//     //    cout << n[ 0 ]->id()<< ": " << n[ 1 ]->id()<< ": " << n[ 2 ]->id()<< ": " << n[ 3 ]->id()<< ": ";
//     if ( ( n[ 4 ] = nodeMatrix.val( n[ 0 ]->id(), n[ 1 ]->id() ) ) == NULL ){
//       n[ 4 ] = createNode( ( n[ 0 ]->pos() + n[ 1 ]->pos() ) / 2.0, nodeCount(), markerT( n[ 0 ], n[ 1 ] ) );
//       nodeMatrix.setVal( n[ 0 ]->id(), n[ 1 ]->id(), n[ 4 ] );
//     }
//     //cout << n[ 4 ]->id()<< ": ";
//     if ( ( n[ 5 ] = nodeMatrix.val( n[ 1 ]->id(), n[ 2 ]->id() ) ) == NULL ){
//       n[ 5 ] = createNode( ( n[ 1 ]->pos() + n[ 2 ]->pos() ) / 2.0, nodeCount(), markerT( n[ 1 ], n[ 2 ] ) );
//       nodeMatrix.setVal( n[ 1 ]->id(), n[ 2 ]->id(), n[ 5 ] );
//     }
//     //cout << n[ 5 ]->id()<< ": ";
//     if ( ( n[ 6 ] = nodeMatrix.val( n[ 2 ]->id(), n[ 0 ]->id() ) ) == NULL ){
//       n[ 6 ] = createNode( ( n[ 2 ]->pos() + n[ 0 ]->pos() ) / 2.0, nodeCount(), markerT( n[ 2 ], n[ 0 ] ) );
//       nodeMatrix.setVal( n[ 2 ]->id(), n[ 0 ]->id(), n[ 6 ] );
//     }
//     //cout << n[ 6 ]->id()<< ": ";
//     if ( ( n[ 7 ] = nodeMatrix.val( n[ 0 ]->id(), n[ 3 ]->id() ) ) == NULL ){
//       n[ 7 ] = createNode( ( n[ 0 ]->pos() + n[ 3 ]->pos() ) / 2.0, nodeCount(), markerT( n[ 0 ], n[ 3 ] ) );
//       nodeMatrix.setVal( n[ 0 ]->id(), n[ 3 ]->id(), n[ 7 ] );
//     }
//     //cout << n[ 7 ]->id()<< ": ";
//     if ( ( n[ 8 ] = nodeMatrix.val( n[ 1 ]->id(), n[ 3 ]->id() ) ) == NULL ){
//       n[ 8 ] = createNode( ( n[ 1 ]->pos() + n[ 3 ]->pos() ) / 2.0, nodeCount(), markerT( n[ 1 ], n[ 3 ] ) );
//       nodeMatrix.setVal( n[ 1 ]->id(), n[ 3 ]->id(), n[ 8 ] );
//     }
//     //cout << n[ 8 ]->id()<< ": ";
//     if ( ( n[ 9 ] = nodeMatrix.val( n[ 2 ]->id(), n[ 3 ]->id() ) ) == NULL ){
//       n[ 9 ] = createNode( ( n[ 2 ]->pos() + n[ 3 ]->pos() ) / 2.0, nodeCount(), markerT( n[ 2 ], n[ 3 ] ) );
//       nodeMatrix.setVal( n[ 2 ]->id(), n[ 3 ]->id(), n[ 9 ] );
//     }
//     //cout << n[ 9 ]->id()<< ": " << endl;

//     for ( int j = 4; j < 10; j ++ ) pNodeVector.push_back( n[ j ] );

//     createTetrahedron( pNodeVector, this->cellCount(), mesh.cell( i ).attribute() );

//     face = findBoundary( mesh.cell( i ).node( 0 ), mesh.cell( i ).node( 1 ), mesh.cell( i ).node( 2 ) );

//     if ( face != NULL ){
//       if ( face->marker() != 0 ) {
// 	pNodeVector.clear();
// 	pNodeVector.push_back( n[ 0 ] ); pNodeVector.push_back( n[ 1 ] ); pNodeVector.push_back( n[ 2 ] );
// 	pNodeVector.push_back( n[ 4 ] ); pNodeVector.push_back( n[ 5 ] ); pNodeVector.push_back( n[ 6 ] );

// 	createTriangleFace( pNodeVector, this->boundaryCount(), face->marker() );
//       }
//     }

//     face = findBoundary( mesh.cell( i ).node( 0 ), mesh.cell( i ).node( 1 ), mesh.cell( i ).node( 3 ) );
//     if ( face != NULL ){
//       if ( face->marker() != 0 ) {
// 	pNodeVector.clear();
// 	pNodeVector.push_back( n[ 0 ] ); pNodeVector.push_back( n[ 1 ] ); pNodeVector.push_back( n[ 3 ] );
// 	pNodeVector.push_back( n[ 4 ] ); pNodeVector.push_back( n[ 8 ] ); pNodeVector.push_back( n[ 7 ] );

// 	createTriangleFace( pNodeVector, this->boundaryCount(), face->marker() );
//       }
//     }

//     face = findBoundary( mesh.cell( i ).node( 0 ), mesh.cell( i ).node( 3 ), mesh.cell( i ).node( 2 ) );
//     if ( face != NULL ){
//       if ( face->marker() != 0 ) {
// 	pNodeVector.clear();
// 	pNodeVector.push_back( n[ 0 ] ); pNodeVector.push_back( n[ 3 ] ); pNodeVector.push_back( n[ 2 ] );
// 	pNodeVector.push_back( n[ 7 ] ); pNodeVector.push_back( n[ 9 ] ); pNodeVector.push_back( n[ 6 ] );

// 	createTriangleFace( pNodeVector, this->boundaryCount(), face->marker() );
//       }
//     }

//     face = findBoundary( mesh.cell( i ).node( 1 ), mesh.cell( i ).node( 2 ), mesh.cell( i ).node( 3 ) );
//     if ( face != NULL ){
//       if ( face->marker() != 0 ) {
// 	pNodeVector.clear();
// 	pNodeVector.push_back( n[ 1 ] ); pNodeVector.push_back( n[ 2 ] ); pNodeVector.push_back( n[ 3 ] );
// 	pNodeVector.push_back( n[ 5 ] ); pNodeVector.push_back( n[ 9 ] ); pNodeVector.push_back( n[ 8 ] );

// 	createTriangleFace( pNodeVector, this->boundaryCount(), face->marker() );
//       }
//     }
//   }
// }

void Mesh3D::createP2Mesh( const BaseMesh & mesh ){
  clear();

  for ( int i = 0, imax = mesh.nodeCount(); i < imax; i ++ ) this->createNode( mesh.node( i ) );

  SparcepNodeMatrix nodeMatrix( nodeCount() );
  vector < Node * > pNodeVector;

  Node *n0 = NULL, *n1 = NULL, *n2 = NULL, *n3 = NULL, *n4 = NULL;
  Node *n5 = NULL, *n6 = NULL, *n7 = NULL, *n8 = NULL, *n9 = NULL;

  BaseElement * face = NULL;

  for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
    pNodeVector.clear();
    n0 = &node( mesh.cell( i ).node( 0 ).id() );
    n1 = &node( mesh.cell( i ).node( 1 ).id() );
    n2 = &node( mesh.cell( i ).node( 2 ).id() );
    n3 = &node( mesh.cell( i ).node( 3 ).id() );

    pNodeVector.push_back( n0 ); pNodeVector.push_back( n1 );
    pNodeVector.push_back( n2 ); pNodeVector.push_back( n3 );

    //    cout << n0->id()<< ": " << n1->id()<< ": " << n2->id()<< ": " << n3->id()<< ": ";
    if ( ( n4 = nodeMatrix.val( n0->id(), n1->id() ) ) == NULL ){
      n4 = createNode( ( n0->pos() + n1->pos() ) / 2.0, nodeCount(), markerT(n0,n1) );
      nodeMatrix.setVal( n0->id(), n1->id(), n4 );
    }
    //cout << n4->id()<< ": ";
    if ( ( n5 = nodeMatrix.val( n0->id(), n2->id() ) ) == NULL ){
      n5 = createNode( ( n0->pos() + n2->pos() ) / 2.0, nodeCount(), markerT(n0,n2) );
      nodeMatrix.setVal( n0->id(), n2->id(), n5 );
    }
    //cout << n5->id()<< ": ";
    if ( ( n6 = nodeMatrix.val( n0->id(), n3->id() ) ) == NULL ){
      n6 = createNode( ( n0->pos() + n3->pos() ) / 2.0, nodeCount(), markerT(n0,n3) );
      nodeMatrix.setVal( n0->id(), n3->id(), n6 );
    }
    //cout << n6->id()<< ": ";
    if ( ( n7 = nodeMatrix.val( n1->id(), n2->id() ) ) == NULL ){
      n7 = createNode( ( n1->pos() + n2->pos() ) / 2.0, nodeCount(), markerT(n1,n2) );
      nodeMatrix.setVal( n1->id(), n2->id(), n7 );
    }
    //cout << n7->id()<< ": ";
    if ( ( n8 = nodeMatrix.val( n2->id(), n3->id() ) ) == NULL ){
      n8 = createNode( ( n2->pos() + n3->pos() ) / 2.0, nodeCount(), markerT(n2,n3) );
      nodeMatrix.setVal( n2->id(), n3->id(), n8 );
    }
    //cout << n8->id()<< ": ";
    if ( ( n9 = nodeMatrix.val( n3->id(), n1->id() ) ) == NULL ){
      n9 = createNode( ( n3->pos() + n1->pos() ) / 2.0, nodeCount(), markerT(n3,n1) );
      nodeMatrix.setVal( n3->id(), n1->id(), n9 );
    }
    //cout << n9->id()<< ": " << endl;

    pNodeVector.push_back( n4 ); pNodeVector.push_back( n5 ); pNodeVector.push_back( n6 ); pNodeVector.push_back( n7 ); pNodeVector.push_back( n8 ); pNodeVector.push_back( n9 );
    createTetrahedron( pNodeVector, this->cellCount(), mesh.cell( i ).attribute() );

    face = findBoundary( mesh.cell( i ).node( 0 ),
			 mesh.cell( i ).node( 1 ),
			 mesh.cell( i ).node( 2 ) );

    if ( face != NULL ){
      if ( face->marker() != 0 ) {
	pNodeVector.clear();
	pNodeVector.push_back( n0 ); pNodeVector.push_back( n1 ); pNodeVector.push_back( n2 );
	pNodeVector.push_back( n4 ); pNodeVector.push_back( n7 ); pNodeVector.push_back( n5 );

	createTriangleFace( pNodeVector, this->boundaryCount(), face->marker() );
      }
    }

    face = findBoundary( mesh.cell( i ).node( 0 ),
			 mesh.cell( i ).node( 1 ),
			 mesh.cell( i ).node( 3 ) );
    if ( face != NULL ){
      if ( face->marker() != 0 ) {
	pNodeVector.clear();
	pNodeVector.push_back( n0 ); pNodeVector.push_back( n1 ); pNodeVector.push_back( n3 );
	pNodeVector.push_back( n4 ); pNodeVector.push_back( n9 ); pNodeVector.push_back( n6 );

	createTriangleFace( pNodeVector, this->boundaryCount(), face->marker() );
      }
    }

    face = findBoundary( mesh.cell( i ).node( 0 ),
			 mesh.cell( i ).node( 3 ),
			 mesh.cell( i ).node( 2 ) );
    if ( face != NULL ){
      if ( face->marker() != 0 ) {
	pNodeVector.clear();
	pNodeVector.push_back( n0 ); pNodeVector.push_back( n3 ); pNodeVector.push_back( n2 );
	pNodeVector.push_back( n6 ); pNodeVector.push_back( n8 ); pNodeVector.push_back( n5 );

	createTriangleFace( pNodeVector, this->boundaryCount(), face->marker() );
      }
    }

    face = findBoundary( mesh.cell( i ).node( 1 ),
			 mesh.cell( i ).node( 2 ),
			 mesh.cell( i ).node( 3 ) );
    if ( face != NULL ){
      if ( face->marker() != 0 ) {
	pNodeVector.clear();
	pNodeVector.push_back( n1 ); pNodeVector.push_back( n2 ); pNodeVector.push_back( n3 );
	pNodeVector.push_back( n7 ); pNodeVector.push_back( n8 ); pNodeVector.push_back( n9 );

	createTriangleFace( pNodeVector, this->boundaryCount(), face->marker() );
      }
    }
  }
}

void Mesh3D::createH2Mesh( const BaseMesh & mesh ){
  clear();

  // alle OrginalKnoten werden kopiert
  for ( int i = 0, imax = mesh.nodeCount(); i < imax; i ++ ){
    createNode( mesh.node( i ) );
  }
  Node *n0 = NULL, *n1 = NULL, *n2 = NULL, *n3 = NULL, *n4 = NULL;
  Node *n5 = NULL, *n6 = NULL, *n7 = NULL, *n8 = NULL, *n9 = NULL;
  BaseElement * face = NULL;

  SparcepNodeMatrix nodeMatrix( nodeCount() );

  for ( int i = 0, imax = mesh.cellCount(); i < imax; i ++ ){
    //    std::cout << "boundcount: " << this->faceCount() << " " << mesh.boundaryCount()<< std::endl;
    n0 = &node( mesh.cell( i ).node( 0 ).id() );
    n1 = &node( mesh.cell( i ).node( 1 ).id() );
    n2 = &node( mesh.cell( i ).node( 2 ).id() );
    n3 = &node( mesh.cell( i ).node( 3 ).id() );

    //    cout << n0->id()<< ": " << n1->id()<< ": " << n2->id()<< ": " << n3->id()<< ": ";
    if ( ( n4 = nodeMatrix.val( n0->id(), n1->id() ) ) == NULL ){
      n4 = createNode( ( n0->pos() + n1->pos() ) / 2.0, nodeCount(), markerT(n0,n1) );
      nodeMatrix.setVal( n0->id(), n1->id(), n4 );
    }
    //cout << n4->id()<< ": ";
    if ( ( n5 = nodeMatrix.val( n0->id(), n2->id() ) ) == NULL ){
      n5 = createNode( ( n0->pos() + n2->pos() ) / 2.0, nodeCount(), markerT(n0,n2) );
      nodeMatrix.setVal( n0->id(), n2->id(), n5 );
    }
    //cout << n5->id()<< ": ";
    if ( ( n6 = nodeMatrix.val( n0->id(), n3->id() ) ) == NULL ){
      n6 = createNode( ( n0->pos() + n3->pos() ) / 2.0, nodeCount(), markerT(n0,n3) );
      nodeMatrix.setVal( n0->id(), n3->id(), n6 );
    }
    //cout << n6->id()<< ": ";
    if ( ( n7 = nodeMatrix.val( n1->id(), n2->id() ) ) == NULL ){
      n7 = createNode( ( n1->pos() + n2->pos() ) / 2.0, nodeCount(), markerT(n1,n2) );
      nodeMatrix.setVal( n1->id(), n2->id(), n7 );
    }
    //cout << n7->id()<< ": ";
    if ( ( n8 = nodeMatrix.val( n2->id(), n3->id() ) ) == NULL ){
      n8 = createNode( ( n2->pos() + n3->pos() ) / 2.0, nodeCount(), markerT(n2,n3) );
      nodeMatrix.setVal( n2->id(), n3->id(), n8 );
    }
    //cout << n8->id()<< ": ";
    if ( ( n9 = nodeMatrix.val( n1->id(), n3->id() ) ) == NULL ){
      n9 = createNode( ( n1->pos() + n3->pos() ) / 2.0, nodeCount(), markerT(n1,n3) );
      nodeMatrix.setVal( n1->id(), n3->id(), n9 );
    }
    //cout << n9->id()<< ": " << endl;

    createTetrahedron( *n4, *n6, *n5, *n0, mesh.cellCount(), mesh.cell( i ).attribute() );
    createTetrahedron( *n4, *n5, *n6, *n9, mesh.cellCount(), mesh.cell( i ).attribute() );

    createTetrahedron( *n7, *n9, *n4, *n1, mesh.cellCount(), mesh.cell( i ).attribute() );
    createTetrahedron( *n7, *n4, *n9, *n5, mesh.cellCount(), mesh.cell( i ).attribute() );

    createTetrahedron( *n8, *n7, *n5, *n2, mesh.cellCount(), mesh.cell( i ).attribute() );
    createTetrahedron( *n8, *n5, *n7, *n9, mesh.cellCount(), mesh.cell( i ).attribute() );

    createTetrahedron( *n6, *n9, *n8, *n3, mesh.cellCount(), mesh.cell( i ).attribute() );
    createTetrahedron( *n6, *n8, *n9, *n5, mesh.cellCount(), mesh.cell( i ).attribute() );

    face = findBoundary( mesh.cell( i ).node( 0 ), mesh.cell( i ).node( 1 ), mesh.cell( i ).node( 2 ) );
    if ( face != NULL ){
      if ( face->marker() != 0 ) {
	createTriangleFace( *n0, *n5, *n4, faceCount(), face->marker() );
	createTriangleFace( *n1, *n4, *n7, faceCount(), face->marker() );
	createTriangleFace( *n2, *n7, *n5, faceCount(), face->marker() );
	createTriangleFace( *n4, *n5, *n7, faceCount(), face->marker() );
      }
    }
    face = findBoundary( mesh.cell( i ).node( 0 ), mesh.cell( i ).node( 1 ), mesh.cell( i ).node( 3 ) );
    if ( face != NULL ){
      if ( face->marker() != 0 ) {
	createTriangleFace( *n0, *n4, *n6, faceCount(), face->marker() );
	createTriangleFace( *n1, *n9, *n4, faceCount(), face->marker() );
	createTriangleFace( *n3, *n6, *n9, faceCount(), face->marker() );
	createTriangleFace( *n4, *n9, *n6, faceCount(), face->marker() );
      }
    }
    face = findBoundary( mesh.cell( i ).node( 1 ), mesh.cell( i ).node( 2 ), mesh.cell( i ).node( 3 ) );
    if ( face != NULL ){
      if ( face->marker() != 0 ) {
	createTriangleFace( *n1, *n7, *n9, faceCount(), face->marker() );
	createTriangleFace( *n2, *n8, *n7, faceCount(), face->marker() );
	createTriangleFace( *n3, *n9, *n8, faceCount(), face->marker() );
	createTriangleFace( *n7, *n8, *n9, faceCount(), face->marker() );
      }
    }
    face = findBoundary( mesh.cell( i ).node( 2 ), mesh.cell( i ).node( 0 ), mesh.cell( i ).node( 3 ) );
    if ( face != NULL ){
      if ( face->marker() != 0 ) {
	createTriangleFace( *n2, *n5, *n8, faceCount(), face->marker() );
	createTriangleFace( *n3, *n8, *n6, faceCount(), face->marker() );
	createTriangleFace( *n0, *n6, *n5, faceCount(), face->marker() );
	createTriangleFace( *n8, *n5, *n6, faceCount(), face->marker() );
      }
    }
  }

  int marker = 0;
  for ( int i = 0; i < faceCount(); i ++ ){
    marker = boundary( i ).marker();
    if ( marker != 0 ){
      if ( boundary( i ).node( 0 ).marker() > -99 ) boundary( i ).node( 0 ).setMarker( marker );
      if ( boundary( i ).node( 1 ).marker() > -99 ) boundary( i ).node( 1 ).setMarker( marker );
      if ( boundary( i ).node( 2 ).marker() > -99 ) boundary( i ).node( 2 ).setMarker( marker );
    }
  }

  //  nodeMatrix.save( "noderefine.matrix" );

  //   for ( int i = 0; i < cellCount(); i ++ ){
  //     if  ( element(i).jacobianDeterminant( ) < 0 ){
  //       cout << i << ": " << element(i).jacobianDeterminant( ) << " ";
  //     }
  //   }
}

int Mesh3D::load( const string & fbody, const string & style, IOFormat format ){;
  if ( format == Binary || fbody.find( MESHBINSUFFIX ) != (size_t)-1 ) return loadBinary( fbody );

  string fileName( fbody ), fileStyle( style );
  if ( fileStyle == "noDef" ){
    if ( (int)fileName.find( ".vol" ) != -1 ){
      if ( verbose_ ) cout << "Assuming 'NetGen' mesh." << endl;
      fileStyle = "NetGen";
    }
  }

  if ( fileStyle == "DCFEMLib" || fileStyle == "d" || fileStyle == "D" || style == "m" || style == "M" ){
    return loadDCFEMLibMesh( fileName );
  } else if ( fileStyle == "Tetgen" || fileStyle == "t" || fileStyle == "T"
	      || fileStyle == "TetGen" ){
    return loadTetGenMesh( fileName );
  } else if ( fileStyle == "FEMLAB" || fileStyle == "f" || fileStyle == "F" ){
    return loadFEMLABMesh( fileName );
  } else if ( fileStyle == "Grummp" || fileStyle == "g" || fileStyle == "G" ){
    return loadGrummpMesh( fileName );
  } else if ( fileStyle == "NetGen" || fileStyle == "n" || fileStyle == "N" ){
    return loadNetGenMesh( fileName );
  } else {
    cerr << "Intputformat: " << fileStyle << " unknown. I can't determine inputformat" << endl;
    return 0;
  }
  return 0;
}

// int Mesh3D::save( const string & fbody ){
//   cout << WHERE_AM_I << endl;
//   BaseMesh::save( fbody );

//   return saveFaces( fbody + ".f" );
// }

// int Mesh3D::loadMyFEMLib( const string & fname ){
//   BaseMesh::load( fname );
//   return loadFaces( fname + ".f" );
// }

// int Mesh3D::loadNodes(const string & fname){
//   fstream file; if ( ! openInFile( fname, & file ) ) {  return 0; }

//   int id, marker;
//   double x, y, z;
//   while( file >> x >> y >> z >> marker ){
//     createNode( x, y, z, nodeCount(), marker );
//   }
//   file.close();
//   return 1;
// }
// int Mesh3D::saveNodes(const string & fname){
//   fstream file;  if ( !openOutFile( fname, & file ) ) {  return 0; }

//   file.setf( ios::scientific, ios::floatfield );
//   file.precision( 8 );

//   //** X       Y      Z      Marker;
//   for ( VectorpNodes::iterator it = pNodeVector_.begin(); it != pNodeVector_.end(); it++ )
//     file << (*it)->x() << "\t" << (*it)->y() << "\t" << (*it)->z() << "\t" << (*it)->marker() << endl;
//   file.close();
//   return 1;
// }

// int Mesh3D::loadFaces( const string & fname ){
//   int columnCount = countColumns( fname );
//   fstream file; if ( !openInFile( fname, & file ) ) {  return 0; }

//   /* Nd1 Nd2 Nd3 Marker */
//   int nd0, nd1, nd2, intDummy = 0, marker = 0;
//   vector < Node * >  pNodeVector;
//   while( file >> nd0 >> nd1 >> nd2 ){
//     pNodeVector.clear();
//     pNodeVector.push_back( &node( nd0 ) ); pNodeVector.push_back( &node( nd1 ) );
//     pNodeVector.push_back( &node( nd2 ) );
//     if ( columnCount == 9 ){
//       file >> nd0 >> nd1 >> nd2;
//       pNodeVector.push_back( &node( nd0 ) ); pNodeVector.push_back( &node( nd1 ) );
//       pNodeVector.push_back( &node( nd2 ) );
//     }
//     file >> intDummy >> intDummy >> marker;
//     createTriangleFace( pNodeVector, this->boundaryCount(), marker );
//   }
//   file.close();
//   return 1;
// }
// int Mesh3D::saveFaces( const string & fname ){
//   fstream file; if ( !openOutFile( fname, & file ) ) {  return 0; }

//   /* Nd1 Nd2 Nd3 Marker */
//   for ( int i = 0, imax = boundaryCount(); i < imax; i++){
//     //    cout << i << "\t" << face( i ).nodeCount() << endl;
//     for ( int j = 0, jmax = boundary( i ).nodeCount(); j < jmax; j++){
//       //  cout << j << "\t" << face( i ).node( j ).id() << endl;
//       file << boundary( i ).node( j ).id() << "\t" ;
//     }

// //     if ( dynamic_cast<TriangleFace&>( boundary( i ) ).leftNeighbour() != NULL )
// //       file << dynamic_cast<TriangleFace&>( boundary( i ) ).leftNeighbour()->id();
// //     else file << "-1";
// //     file << "\t";
// //     if ( dynamic_cast<TriangleFace&>( boundary( i ) ).rightNeighbour() != NULL )
// //       file << dynamic_cast<TriangleFace&>( boundary( i ) ).rightNeighbour()->id();
// //     else file << "-1";

//     file << "-1\t-1\t";
//     file << boundary( i ).marker() << endl;
//   }
//   file.close();
//   return 1;
// }

// int Mesh3D::loadElements( const string & fname){
//   int columnCount = countColumns( fname );
//   fstream file; if ( !openInFile( fname, & file ) ) {  return 0; }

//   /* Nd1 Nd2 Nd3 Nd4 Attribute */
//   int nd0 = 0, nd1 = 0, nd2 = 0, nd3 = 0, ndi = 0;
//   double attribute = 0.0;

//   vector< Node * > pNodeVector;

//   while( file >> nd0 >> nd1 >> nd2 >> nd3 ){
//     pNodeVector.clear();
//     pNodeVector.push_back( &node( nd0 ) );
//     pNodeVector.push_back( &node( nd1 ) );
//     pNodeVector.push_back( &node( nd2 ) );
//     pNodeVector.push_back( &node( nd3 ) );
//     if ( columnCount == 11 ){
//       for ( int i = 0; i < 6; i ++ ){
// 	file >> ndi;
// 	pNodeVector.push_back( &node( ndi ) );
//       }
//     }
//     file >> attribute;
//     createTetrahedron( pNodeVector, cellCount(), attribute );
//   }
//   file.close();
//   return 1;
// }
// int Mesh3D::saveElements( const string & fname){
//   fstream file; if ( !openOutFile( fname, & file ) ) {  return 0; }

//   /* Nd1 Nd2 Nd3 Nd4 Attribute */
//   for ( int i = 0, imax = cellCount(); i < imax; i++){
//     for ( int j = 0, jmax = cell( i ).nodeCount(); j < jmax; j++){
//       file << cell( i ).node( j ).id() << "\t" ;
//     }
//     file << cell( i ).attribute() << endl;
//   }
//   file.close();
//   return 1;
// }


int Mesh3D::loadTETGENMesh( const string & fbody, bool withFace ){
  cerr << WHERE_AM_I << " Dirty, use loadTetGenMesh( fbody, withFace ) instead" << endl;
  return loadTetGenMesh( fbody, withFace );
}

int Mesh3D::loadTetGenNodes( const string & fbody ){
  fstream file; if ( ! openInFile( fbody, & file ) ) { return 0; }

  int nverts = 0, dummy = 0, ifMarker = 0, marker = 0;
  file >> nverts >> dummy >> dummy >> ifMarker;
  double x = 0.0, y = 0.0, z = 0.0;

  int vcountmax = (long)rint(nverts / 100), vcount = 0;
  for ( int i = 0; i < nverts; i ++, vcount++ ){
    if ( verbose_ ){
      if ( vcount == vcountmax ){
	cout << "\r" << (int)ceil( ( i * 100.0 ) / ( nverts ) ) << " % "; vcount = 0;
      }
    }
    file >> dummy >> x >> y >> z; if ( ifMarker ) file >> marker;
    createNode( x, y, z, i, marker );
  }
  if ( verbose_ ) cout << endl;
  file.close();
  return 1;
}

int Mesh3D::loadTetGenMesh( const string & fbody, bool withFace, bool tet10node ){;
  clear();

  if ( !loadTetGenNodes( fbody + ".node" ) ) return 0;

  int ncells = 0, dim = 0, nfaces = 0, nbfaces = 0, marker = 0, ifMarker = 0, dummy = 0;
  int vcountmax = 0, vcount = 0;

  int n0 = 0, n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0, n7 = 0, n8 = 0, n9 = 0;
  fstream file;

  if ( openInFile( fbody + ".ele", & file ) ){
    file >> ncells >> dim >> ifMarker;

    double attribute = 0.0;
    vector < Node * > pNodeVector;

    vcountmax = (long)rint(ncells / 100);
    vcount = 0;
    for ( int i = 0; i < ncells; i ++, vcount ++ ){
      if ( verbose_ ) {
	if ( vcount == vcountmax ){
	  cout << "\r" << (int)ceil( ( i * 100.0 ) / ( ncells ) ) << " % "; vcount = 0;
	}
      }
      pNodeVector.clear();
      file >> dummy >> n0 >> n1 >> n2 >> n3;
      pNodeVector.push_back( &node( n0 ) ); pNodeVector.push_back( &node( n1 ) );
      pNodeVector.push_back( &node( n2 ) ); pNodeVector.push_back( &node( n3 ) );

      if ( dim == 10 ) {
	file >> n4 >> n5 >> n6 >> n7 >> n8 >> n9;
	pNodeVector.push_back( &node( n4 ) );
	pNodeVector.push_back( &node( n6 ) );
	pNodeVector.push_back( &node( n7 ) );
	pNodeVector.push_back( &node( n5 ) );
	pNodeVector.push_back( &node( n9 ) );
	pNodeVector.push_back( &node( n8 ) );
      }
      if ( ifMarker ) file >> attribute;
      createTetrahedron( pNodeVector, i, attribute );
    }
    if ( verbose_ ) cout << endl;
    file.close();
  } else { // no ele-file found
    withFace = true;
  }

  if ( ! openInFile( fbody + ".face", & file ) ) { return 0; }
  file >> nfaces >> ifMarker;
  BaseElement * tri;

  vcountmax = (long)rint( nfaces / 100 ); vcount = 0;
  for ( int i = 0; i < nfaces; i ++, vcount++ ){
    if ( verbose_ ) {
      if ( vcount == vcountmax ){
	cout << "\r" << (int)ceil( ( i * 100.0 ) / ( nfaces ) ) << " % "; vcount = 0;
      }
    }
    if ( tet10node ) {
      TO_IMPL
    } else {
      file >> dummy >> n0 >> n1 >> n2; if ( ifMarker ) file >> marker;
      // cout << dummy << "\t" << a  << "\t" << b  << "\t" << c << endl;

      if ( marker != 0 || withFace){
          
    // check here !!!!!!!!!!!!!!!1
    // if face norm vector is showing outward. Tetgen ignore input direction.!!!!!!!!!
    // All normal vectors showing inside .. there is difference between maxarea == 0 and maxarea != 0
    // GIMLI need all normal vector showing outside
     
	tri = createTriangleFace(node(n0), node(n1), node(n2), i, marker );
    // check here !!!!!!!!!!!!!!!1
    
	for ( int i = 0; i < 3; i ++ ){
	  if ( tri->node( i ).marker() > -90){
	    tri->node( i ).setMarker( marker );
	  }
	}
      }
    }
  } // for each face
  if ( verbose_ ) cout << endl;
  file.close();
  //  cout << WHERE_AM_I << "no neighbours loaded" << endl;
  return 1;
  
  
  

  int nNeigh = 0, intDummy = 0, neighbour = 0;
  if ( ! openInFile( fbody + ".neigh", & file ) ) { return 0; }
  BaseElement * face = NULL;
  RealPos normtmp;
  file >> nNeigh >> intDummy;
  set < Tetrahedron * > tetsToCheck;

  vcountmax = (long)rint( nNeigh / 100 ); vcount = 0;
  for ( int i = 0; i < nNeigh; i ++, vcount++ ){
    if ( vcount == vcountmax ){
      cout << "\r" << (int)ceil( ( i * 100.0 ) / ( nNeigh ) ) << " % "; vcount = 0;
    }
    file >> intDummy;

    for ( int j = 0; j < 4;  j ++ ){
      file >> neighbour;
      if ( neighbour == -1 ){
	// nicht eindeutig wegen widerspruechen in der TetGen-Doku, die facets der Tets werden spaeter gecheckt
	tetsToCheck.insert( dynamic_cast< Tetrahedron * > ( &cell( i ) ) );
      } else {
	face = findBoundary( cell( i ), element( neighbour ) );
	if ( face != NULL ) {
	  if ( ( face->normVector() - cell( i ).averagePos() ).abs() <
	       ( face->normVector() - element( neighbour ).averagePos() ).abs() ){
	    dynamic_cast<TriangleFace *>(face)->setLeftNeighbour( dynamic_cast< Tetrahedron * > ( & cell( i )));
	    dynamic_cast<TriangleFace *>(face)->setRightNeighbour( dynamic_cast< Tetrahedron * > ( &element( neighbour )));
	  } else{
	    dynamic_cast<TriangleFace *>(face)->setRightNeighbour( dynamic_cast< Tetrahedron * > ( & cell( i )));
	    dynamic_cast<TriangleFace *>(face)->setLeftNeighbour( dynamic_cast< Tetrahedron * > ( &element( neighbour )));
	  }
	}
      }
    }
  }
  for ( set < Tetrahedron * >::iterator it = tetsToCheck.begin(); it != tetsToCheck.end(); it ++){
    face = findBoundary( (*it)->node( 0 ), (*it)->node( 1 ), (*it)->node( 2 ) );
    if ( dynamic_cast<TriangleFace *>(face)->leftNeighbour() == NULL ) dynamic_cast<TriangleFace *>(face)->setLeftNeighbour( (*it) );
    face = findBoundary( (*it)->node( 0 ), (*it)->node( 1 ), (*it)->node( 3 ) );
    if ( dynamic_cast<TriangleFace *>(face)->leftNeighbour() == NULL ) dynamic_cast<TriangleFace *>(face)->setLeftNeighbour( (*it) );
    face = findBoundary( (*it)->node( 1 ), (*it)->node( 2 ), (*it)->node( 3 ) );
    if ( dynamic_cast<TriangleFace *>(face)->leftNeighbour() == NULL ) dynamic_cast<TriangleFace *>(face)->setLeftNeighbour( (*it) );
    face = findBoundary( (*it)->node( 2 ), (*it)->node( 0 ), (*it)->node( 3 ) );
    if ( dynamic_cast<TriangleFace *>(face)->leftNeighbour() == NULL ) dynamic_cast<TriangleFace *>(face)->setLeftNeighbour( (*it) );
  }
  cout << endl;
  file.close();
  return 1;
}

int Mesh3D::loadGRUMPMesh( const string & fbody ){;
  // newfile vmesh
  // ncells nfaces nbfaces nverts
  // verts: coords
  // faces: cells verts
  // bfaces: face bc verts
  // cells: verts region
  clear();
  //  fstream file; if ( ! openInFile( fbody, & file ) ) { return 0; }
  fstream file; if ( ! openInFile( fbody + ".vmesh", & file ) ) { return 0; }

  int ncells = 0, nfaces = 0, nbfaces = 0, nverts = 0;
  int marker = 0;
  double x, y, z;

  //  ncells nfaces nbfaces nverts
  Node * vert;
  file >> ncells >> nfaces >> nbfaces >> nverts;
  for ( int i = 0, imax = nverts; i < imax; i++ ){
    //verts: coords
    file >> x >> y >> z;
    vert = createNode( x, y, z, i, marker );
  }

  int dummy = 0;
  int a = 0, b = 0, c = 0, d = 0;
  for ( int i = 0, imax = nfaces; i < imax; i++ ){
    // faces: cells verts
    file >> dummy >> dummy >> a >> b >> c;
    createTriangleFace( node(a), node(b), node(c), i, 0);
  }

  int faceid = 0;
  for ( int i = 0, imax = nbfaces; i < imax; i++ ){
    //bfaces: face bc verts
    file >> faceid >> marker >> a >> b >> c ;
    face( faceid ).setMarker( marker );
  }

  for ( int i = 0, imax = ncells; i < imax; i++ ){
    //cells: verts region
    file >> a >> b >> c >> d >> marker;
    createTetrahedron( node(a), node(b), node(c), node(d), i, marker );
  }

  file.close();
  return 1;
}

int Mesh3D::loadUserGRUMPMesh( const string & fbody ){
//     verts: coords
//     cells: verts region
  clear();
  fstream file; if ( ! openInFile( fbody, & file ) ) {  return 0; }

  int ncells = 0, nfaces = 0, nbfaces = 0, nverts = 0;
  int marker = 0;
  double x, y, z;

  file >> nverts >> ncells >> nfaces >> nbfaces ;
  Node * vert;

  for ( int i = 0, imax = nverts; i < imax; i++ ){
    file >> x >> y >> z;
    vert = createNode( x, y, z, i, marker );
  }

  int a = 0, b = 0, c = 0, d = 0;

  for ( int i = 0, imax = ncells; i < imax; i++ ){
    file >> a >> b >> c >> d >> marker;
    createTetrahedron( node(a), node(b), node(c), node(d), i, marker );
  }


  file.close();

  return 1;
}

int Mesh3D::loadFEMLABMesh( const string & fbody ){
  clear();
  fstream file; if ( ! openInFile( fbody + ".node", & file ) ) { return 0; }

  double x, y, z;
  Node * vert;
  while ( file >> x >> y >> z ){
    vert = createNode( x, y, z, nodeCount(), 0 );
  }
  file.close();

  fstream elefile; if ( ! openInFile( fbody + ".ele", & elefile ) ) { return 0; }
  double a = 0, b = 0, c = 0, d = 0;
  double attribute = 0.0;
  while( elefile >> a >> b >> c >> d >> attribute ){
    createTetrahedron( node((long)a-1), node((long)b-1), node((long)c-1), node((long)d-1), cellCount(), attribute );
  }
  elefile.close();

  vector < int > bndindex;
  fstream bifile; if ( openInFile( fbody + ".face.info", & bifile ) ) {
    double index = 0.0;
    int myindex = 0;
    while( bifile >> index ) bndindex.push_back( (long)index );

    for ( unsigned int i = 0, imax = bndindex.size(); i < imax; i++ ){
      switch( bndindex[ i ] ){
      case 1: myindex = -4; break;
      case 2: myindex = -1; break;
      case 3: myindex = -3; break;
      default: cerr << WHERE_AM_I << " Index klaeren = " << bndindex[ i ] << endl;
      }
      bndindex[ i ] = myindex;
    }
  }
  bifile.close();

  fstream ffile; if ( ! openInFile( fbody + ".face", & ffile ) ) { return 0; }
  BaseElement * tri;
  double u1, v1, u2, v2, u3, v3, bd, sdu, sdd;
  int bdint = 0;
  while( ffile >>  a >> b >> c
	 >> u1 >> v1 >> u2 >> v2 >> u3 >> v3 >> bd >> sdu >> sdd ){
    bdint = ( int )rint( bd );
    tri = createTriangleFace( node( (long)a - 1 ), node( (long)b - 1 ), node( (long)c - 1 ), boundaryCount(), bdint );

    if ( bndindex.size() > 0 ){
      for ( int i = 0; i < 3; i ++ ){
	tri->node( i ).setMarker( bndindex[ bdint - 1 ] );
      }
      tri->setMarker( bndindex[ bdint - 1 ] );
    }
  }
  ffile.close();

  fstream nifile; if ( openInFile( fbody + ".node.info", & nifile ) ) {
    double index = 0.0;
    vector < int > nodeindex;
    while( nifile >> index ) nodeindex.push_back( (long)index );
    for ( unsigned int i = 0, imax = nodeindex.size(); i < imax; i++ ){
      if ( nodeindex[ i ] != 1 ) node( i ).setMarker( -99 );
    }
  }
  nifile.close();
  return 1;
}

int Mesh3D::loadNetGenMesh( const string & fname ){
  TO_IMPL
//   string fileName = fname;
//   clear();
//   if ( (int)fileName.find( ".vol" ) == -1 ) fileName += ".vol";
//   fstream file; if ( ! openInFile( fileName, & file ) ) { return 0; }

//   char buf[1024];
//   for ( int i = 0; i < 6; i ++ ) {
//     file.getline( buf, 1024 );
//   }

//   int nFaces = -1;
//   file >> nFaces; // mesh3d
//   //# surfnr    bcnr   domin  domout      np      p1      p2      p3  dummy dummy dummy
//   int dummy = -1, left = -1,  right = -1, nVertsFace = -1;;
//   int *aF = new int[nFaces];
//   int *bF = new int[nFaces];
//   int *cF = new int[nFaces];
//   int *dF = new int[nFaces];
//   int *eF = new int[nFaces];
//   int *fF = new int[nFaces];
//   int *markerF = new int[nFaces];

//   int vcountmax = (long)rint(nFaces / 100), vcount = 0;
//   for ( int i = 0; i < nFaces; i ++, vcount ++ ){
//     if ( vcount == vcountmax ){
//       cout << "\r" << (int)ceil( ( i * 100.0 ) / ( nFaces ) ) << " % "; vcount = 0; }

//     file >> dummy >> markerF[i] >> left >> right >> nVertsFace;
//     //    cout << dummy << markerF[i] << left << right << nVertsFace << endl;
//     switch ( nVertsFace ){
//     case 3: file >> aF[i] >> bF[i] >> cF[i];
//       file >> dummy >> dummy >> dummy;
//       break;
//     case 6: file >> aF[i] >> bF[i] >> cF[i] >> dF[i] >> eF[i] >> fF[i];
//       file >> dummy >> dummy >> dummy >> dummy >> dummy >> dummy;
//       break;
//     default: cerr << WHERE_AM_I << " format not know. " << nVertsFace << endl;
//     }
//   }
//   cout << endl;

//   for ( int i = 0; i < 5; i ++ ) {
//     file.getline( buf, 1024 );
//   }
//   int nCells = -1;
//   file >> nCells; // mesh3d

//   //#  matnr      np      p1      p2      p3      p4
//   int nVertsCell = -1;
//   int *aC = new int[ nCells ];
//   int *bC = new int[ nCells ];
//   int *cC = new int[ nCells ];
//   int *dC = new int[ nCells ];
//   int *eC = new int[ nCells ];
//   int *fC = new int[ nCells ];
//   int *gC = new int[ nCells ];
//   int *hC = new int[ nCells ];
//   int *iC = new int[ nCells ];
//   int *kC = new int[ nCells ];
//   int *attributeC = new int[ nCells ];

//   vcountmax = (long)rint(nCells / 100); vcount = 0;
//   for ( int i = 0; i < nCells; i ++, vcount ++ ){
//     if ( vcount == vcountmax ){
//       cout << "\r" << (int)ceil( ( i * 100.0 ) / ( nCells ) ) << " % "; vcount = 0; }

//     file >> attributeC[ i ] >> nVertsCell;
//     switch ( nVertsCell ){
//     case 4: file >> aC[i] >> bC[i] >> cC[i] >> dC[i];
//       break;
//     case 10: file >> aC[i] >> bC[i] >> cC[i] >> dC[i]
// 		  >> eC[i] >> fC[i] >> gC[i] >> hC[i] >> iC[i] >> kC[i];
//       break;
//     default: cerr << WHERE_AM_I << " format not know. " << endl;
//     }
//   }
//   cout << endl;

//   for ( int i = 0; i < 5; i ++ ) {
//     file.getline( buf, 1024 );
//   }
//   int nSegmentEdges = -1;
//   file >> nSegmentEdges; // mesh3d
//   for ( int i = 0; i < nSegmentEdges * 2; i ++ ) {
//     file.getline( buf, 1024 );
//   }
//   for ( int i = 0; i < 5; i ++ ) {
//     file.getline( buf, 1024 );
//   }

//   int nVerts = -1;
//   file >> nVerts;
//   double x = 0.0, y = 0.0, z = 0.0;

//   vcountmax = (long)rint(nVerts / 100); vcount = 0;
//   for ( int i = 0; i < nVerts; i ++, vcount ++ ) {
//     if ( vcount == vcountmax ){
//       cout << "\r" << (int)ceil( ( i * 100.0 ) / ( nVerts ) )<< " % "; vcount = 0; }
//     file >> x >> y >> z;
//     createNode( x, y, z, i, 0 );
//   }
//   cout << endl;
//   file.close();

//   int fromNull = 1;
//   vector < Node * > pNodeVector;
//   for ( int i = 0; i < nCells; i ++ ) {
//     pNodeVector.clear();
//     pNodeVector.push_back( &node( aC[ i ] - fromNull) );
//     pNodeVector.push_back( &node( bC[ i ] - fromNull) );
//     pNodeVector.push_back( &node( cC[ i ] - fromNull) );
//     pNodeVector.push_back( &node( dC[ i ] - fromNull) );
//     if ( nVertsCell == 10 ){
//       pNodeVector.push_back( &node( eC[ i ] - fromNull) );
//       pNodeVector.push_back( &node( fC[ i ] - fromNull) );
//       pNodeVector.push_back( &node( gC[ i ] - fromNull) );
//       pNodeVector.push_back( &node( hC[ i ] - fromNull) );

//       pNodeVector.push_back( &node( kC[ i ] - fromNull) );
//       pNodeVector.push_back( &node( iC[ i ] - fromNull) );
//     }
//     createTetrahedron( pNodeVector, i, attributeC[ i ] );

//   }

//   // die jacobimatrix jedes Tet4 ist negativ daher gloabel switchDirection; liegt am netgen-file
//   for ( int i = 0; i < cellCount(); i ++ ) {
//     dynamic_cast<Tetrahedron & >(cell(i)).switchDirection();
//   }

//   for ( int i = 0; i < nFaces; i ++ ) {
//     pNodeVector.clear();
//     pNodeVector.push_back( &node( aF[ i ] - fromNull) );
//     pNodeVector.push_back( &node( bF[ i ] - fromNull) );
//     pNodeVector.push_back( &node( cF[ i ] - fromNull) );
//     if ( nVertsFace == 6 ){
//       pNodeVector.push_back( &node( dF[ i ] - fromNull) );
//       pNodeVector.push_back( &node( eF[ i ] - fromNull) );
//       pNodeVector.push_back( &node( fF[ i ] - fromNull) );
//     }
//     createTriangleFace( pNodeVector, i, markerF[ i ] );
//   }
//   delete [] aF;
//   delete [] bF;
//   delete [] cF;
//   delete [] dF;
//   delete [] eF;
//   delete [] fF;
//   delete [] markerF;

//   delete [] aC;
//   delete [] bC;
//   delete [] cC;
//   delete [] dC;
//   delete [] eC;
//   delete [] fC;
//   delete [] gC;
//   delete [] hC;
//   delete [] iC;
//   delete [] kC;
//  delete [] attributeC;
  return 1;
}

int Mesh3D::loadGetfemMesh( const string & fname ){
  TO_IMPL
//   fstream file; if ( ! openInFile( fileName, & file ) ) { return 0; }

//   char strDummy[ 128 ];

//   file >> strDummy;
//   if  ( strDummy == "POINT" ){
//   }


//   file.close();
  return 1;
}

int Mesh3D::saveFacesGrumpBdry( const string & fname ){
  fstream file; if ( ! openOutFile( fname, & file ) ) return 0;

  file << nodeCount() << "\t" << boundaryCount() << endl;

  for ( int i = 0, imax = nodeCount(); i < imax; i++){
    file << node(i).x() << "\t" << node(i).y() << "\t" << node(i).z() << endl;
  }

  for ( int i = 0, imax = boundaryCount(); i < imax; i++){
    file << "polygon b 1 r 1 3 "
 	 << face( i ).node( 0 ).id() << "\t"
 	 << face( i ).node( 1 ).id() << "\t"
 	 << face( i ).node( 2 ).id() << endl;
  }
  file.close();
  return 1;
}

int Mesh3D::saveFacesOOGL_OffFile( const string & fbody ){
  fstream file; if ( ! openOutFile( fbody+".off", & file ) ) {  return 0; }

  file << "OFF" << endl;
  file << nodeCount() << "\t" << boundaryCount() << "\t" << 0 << endl;

  for ( int i = 0; i < nodeCount(); i ++ ){
    file << node( i ).x() << "\t" << node( i ).y() << "\t" << node( i ).z() << endl;
  }

  for ( int i = 0; i < boundaryCount(); i ++ ){
    file << "3\t"
	 << face( i ).node( 0 ).id() << "\t"
	 << face( i ).node( 1 ).id() << "\t"
	 << face( i ).node( 2 ).id() << endl;
  }
  file.close();
  return 1;
}

int Mesh3D::saveGMVFile( const string & fbody ){
  fstream file; if ( ! openOutFile( fbody + ".gmv", & file ) ) { return 0; }
  file << "gmvinput ascii" << endl;
  file << "comments" << endl;
  file << "# Mesh generated by myfemlib " << WHERE_AM_I << endl;
  file << "endcomm" << endl;

  file << "nodev " << nodeCount() << endl;
  for ( int i = 0, imax = nodeCount(); i < imax; i ++){
    file << node( i ).x() << "\t" << node( i ).y() << "\t" << node( i ).z() << endl;
  }
  file << "cells " << cellCount() << endl;
  for ( int i = 0, imax = cellCount(); i < imax; i ++){
    file << "tet 4" << endl;
    file << cell( i ).node( 0 ).id() + 1 << "\t"
	 << cell( i ).node( 1 ).id() + 1 << "\t"
	 << cell( i ).node( 2 ).id() + 1 << "\t"
	 << cell( i ).node( 3 ).id() + 1 << endl;
  }


  file << "endgmv" << endl;
  file.close();
  return 1;
}

  //int Mesh3D::saveVTKUnstructured( const string & fbody, RVector & data, bool logScaleData ){;
// // bool binary = true;
//     bool binary = false;
//   fstream file; if ( ! openOutFile( fbody + ".vtk", & file ) ) { return 0; }

//   file << "# vtk DataFile Version 3.0" << endl;
//   file << "created by " << WHERE_AM_I << endl;
//   if ( binary ){
//     file << "BINARY" << endl;
//   } else {
//     file << "ASCII" << endl;
//   }
//   file << "DATASET UNSTRUCTURED_GRID" << endl;
//   file << "POINTS " << nodeCount() << " double" << endl;
//   for ( int i = 0; i < nodeCount(); i ++ ){
//     if ( binary ){
//       file.write( (char*)&node( i ).pos()[0], sizeof( double ) );
//       file.write( (char*)&node( i ).pos()[1], sizeof( double ) );
//       file.write( (char*)&node( i ).pos()[2], sizeof( double ) );
//     } else {
//       file << node( i ).pos().x() << "\t"
// 	   << node( i ).pos().y() << "\t"
// 	   << node( i ).pos().z() << endl;
//     }
//   }
//   if ( binary ){ file << endl; }
//   int nCellsSize = cellCount() * ( cell( 0 ).nodeCount() + 1 );
// //   if ( boundaryCount() > 0 ){
// //     nCellsSize += boundaryCount() *( boundary( 0 ).nodeCount() + 1 );
// //   }

//   file << "CELLS " << cellCount() << " " << nCellsSize << endl;
//   //  file << "CELLS " << cellCount() + boundaryCount() << " " << nCellsSize << endl;

//   long iDummy;
//   for ( int i = 0, imax = cellCount(); i < imax; i ++){
//     if ( binary ){
//       iDummy = cell( i ).nodeCount();
//       file.write( (char*)&iDummy, sizeof( iDummy ) );
//     } else {
//       file << cell( i ).nodeCount() << "\t";
//     }
//     for ( int j = 0, jmax = cell( i ).nodeCount(); j < jmax; j ++){
//       if ( binary ){
// 	iDummy = cell( i ).node(j).id();
// 	file.write( (char*)&iDummy, sizeof( iDummy ) );
//       } else {
// 	file << cell( i ).node(j).id() << "\t";
//       }
//     }
//     if ( !binary ) file << endl;
//   }
//   if ( binary ) file << endl;
// //   for ( int i = 0, imax = boundaryCount(); i < imax; i ++){
// //     file << boundary( i ).nodeCount() << "\t";
// //     for ( int j = 0, jmax = boundary( i ).nodeCount(); j < jmax; j ++){
// //       file << boundary( i ).node( j ).id() << "\t";
// //     }
// //     file << endl;
// //   }

//   file << "CELL_TYPES " << cellCount() << endl;
//   iDummy = 10;
//   for ( int i = 0, imax = cellCount(); i < imax; i ++) {
//     if ( binary ){
//       file.write( (char*)&iDummy, sizeof( iDummy ) );
//     } else {
//       file << "10 ";
//     }
//   }
//   file << endl;

// //   file.close();
// //   exit(1);
//   // file << endl;
// //   for ( int i = 0, imax = boundaryCount(); i < imax; i ++){
// //     file << "5 ";
// //   }
//   if (data.size() > 0 ){
//     double value = 0.0;//
//     double eps = epsilon( data );
//     cout << "epsilon :"  << eps << endl;
//     //    eps = 1e-20;

//     RVector scaledValues( data );
//     if ( logScaleData ) {
//       //      scaledValues = absolute(log10( absolute( (data) / eps ) + eps ) )* ( data / absolute(data) ); ;
//       //      scaledValues = absolute( log10( absolute( data / eps ) ) ) * ( data / absolute(data) );
//       //      scaledValues = log10( data );
//       scaledValues = log10( absolute( data ) ) * ( data / absolute(data) );
//     }

// //     file << "CELL_DATA " << cellCount() << endl;
// //     file << "SCALARS Attribute double 1" << endl;
// //     file << "LOOKUP_TABLE default" << endl;

// //     for ( int i = 0, imax = cellCount(); i < imax; i ++) {
// //       if ( binary ){
// // 	file.write( (char*)&scaledValues[ i ], sizeof( double ) );
// //       } else {
// // 	file << cell( i ).attribute() << " ";
// //       }
// //     }
// //     file << endl;

//     if ( data.size() == cellCount() ){
//       file << "CELL_DATA " << cellCount() << endl;
//       file << "SCALARS Resistivity(log10) double 1" << endl;
//       file << "LOOKUP_TABLE default" << endl;

//       for ( int i = 0, imax = cellCount(); i < imax; i ++) {
// 	if ( binary ){
// 	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
// 	} else {
// 	  file << scaledValues[ i ] << " ";
// 	}
//       }
//       file << endl;
//     } else if ( data.size() == nodeCount() ){
//       file << "POINT_DATA " << nodeCount() << endl;
//       file << "SCALARS Potential(log10) double 1" << endl;
//       file << "LOOKUP_TABLE default" << endl;

//       for ( int i = 0, imax = nodeCount(); i < imax; i ++) {
// 	if ( binary ){
// 	  file.write( (char*)&scaledValues[ i ], sizeof( double ) );
// 	} else {
// 	  file << scaledValues[ i ] << " ";
// 	}
//       }
//       file << endl;
//     }
//   }

// //   file << "CELL_DATA " << cellCount() << endl;
// //   file << "SCALARS Material double 1 " << endl;
// //   file << "LOOKUP_TABLE default" << endl;
// //   for ( int i = 0, imax = cellCount(); i < imax; i ++)  file << cell(i).attribute() << " ";
// //   file << endl;

//   file.close();
//   return 1;
// }

int Mesh3D::saveINRIAMeshFile( const string & fbody, RVector & data, bool logScaleData ){
  fstream file; if ( ! openOutFile( fbody + ".mesh", & file ) ) { return 0; }
  file.precision( 14 );
  file << "MeshVersionFormatted 1" << endl;
  file << "Dimension 3" << endl;
  file << "# Mesh generated by myfemlib " << WHERE_AM_I << endl;

  file << "# Set of Mesh Vertices" << endl;
  file << "Vertices" << endl;
  file << nodeCount() << endl;
  for ( int i = 0, imax = nodeCount(); i < imax; i ++){
    file << node( i ).x() << "\t"
	 << node( i ).y() << "\t"
	 << node( i ).z() << "\t" << node( i ).marker() << endl;
  }

  long markercount = 0;
  for ( int i = 0, imax = boundaryCount(); i < imax; i ++) if ( face( i ).marker() != 0 ) markercount ++;

  file << "# Set of Mesh Triangles" << endl;
  file << "Triangles" << endl;
  file << markercount << endl;
  for ( int i = 0, imax = boundaryCount(); i < imax; i ++){
    if ( face( i ).marker() != 0 ){
      file << face( i ).node( 0 ).id() + 1 << "\t"
	   << face( i ).node( 1 ).id() + 1 << "\t"
	   << face( i ).node( 2 ).id() + 1 << "\t" << face( i ).marker() << endl;
    }
  }

  file << "# Set of Mesh Tethrahedrons" << endl;
  file << "Tetrahedra" << endl;
  file << cellCount() << endl;
  for ( int i = 0, imax = cellCount(); i < imax; i ++){
    file << cell( i ).node( 0 ).id() + 1 << "\t"
	 << cell( i ).node( 1 ).id() + 1 << "\t"
	 << cell( i ).node( 2 ).id() + 1 << "\t"
	 << cell( i ).node( 3 ).id() + 1 << "\t" << cell( i ).attribute() << endl;
  }
  file << "End" << endl;
  file.close();

  if ( data.size() > 0 ){
    double value = 0.0;//
    double eps = epsilon( data );
    cout << "epsilon :"  << eps << endl;
    fstream bbfile; if ( ! openOutFile( fbody + ".bb", & bbfile ) ) { return 0; }

    RVector scaledValues( data );
    if ( logScaleData ) {
      eps *= 0.1;
      //      scaledValues = log10( absolute( data ) - eps ) * ( data / absolute(data) );
      scaledValues=log10( absolute(data) + eps );
    }
    if ( data.size() == nodeCount() ){
      bbfile << "3 1 " <<  nodeCount() << " 2" << endl;
    } else if ( data.size() == cellCount() ){
      bbfile << "3 1 " <<  cellCount() + boundaryCount() << " 1" << endl;
    }
    for ( int i = 0, imax = scaledValues.size(); i < imax; i ++){
      bbfile << scaledValues[ i ] << endl;
    }
    if ( data.size() == cellCount() ){
      for ( int i = 0, imax = boundaryCount(); i < imax; i ++) bbfile << "0" << endl;
    }
    bbfile.close() ;
  }

  return 1;
}

int Mesh3D::saveOOGL_Off( const string & fbody, double shrink ){
  //http://www.geomview.org/docs/html/geomview_41.html#SEC44
  fstream file; if ( ! openOutFile( fbody+".off", & file ) ) {  return 0; }

  if ( shrink == 1.) return saveFacesOOGL_OffFile( fbody );

  int nverts = nodeCount();
  int ncells = cellCount();
  int nfaces = pBoundaryVector_.size();
  int nedges = boundaryCount();

  //  if ( ncells == 0 ) { saveFacesOOGL_OffFile( fbody ); return 1; }

  VectorpNodes pNodeVectorTMP;
  VectorpElements pTetVectorTMP;
  VectorpFaces pFaceVectorTMP;
  for (int i = 0; i < ncells; i ++ ){
    for (int j = 0; j < 4; j ++ ){
      pNodeVectorTMP.push_back( new Node( element(i).node(j).x(), element(i).node(j).y(), element(i).node(j).z(), (i*4) + j ) );
    }
    pTetVectorTMP.push_back( new Tetrahedron( *pNodeVectorTMP[ ( i * 4 ) ],
					      *pNodeVectorTMP[ ( i * 4 ) + 1 ],
					      *pNodeVectorTMP[ ( i * 4 ) + 2 ],
					      *pNodeVectorTMP[ ( i * 4 ) + 3 ] ) );
//     cout << pNodeVectorTMP[i]->id() << endl;
//     cout << pNodeVectorTMP[i*4+1]->id() << endl;
//     cout << pNodeVectorTMP[i*4+2]->id() << endl;
//     cout << pNodeVectorTMP[i*4+3]->id() << endl;
  }

  for (unsigned int i = 0; i < pTetVectorTMP.size(); i ++ ){
    pFaceVectorTMP.push_back( new TriangleFace( pTetVectorTMP[i]->node(0),
						pTetVectorTMP[i]->node(1),
						pTetVectorTMP[i]->node(2), i * 4 ) );
    pFaceVectorTMP.push_back( new TriangleFace( pTetVectorTMP[i]->node(0),
						pTetVectorTMP[i]->node(1),
						pTetVectorTMP[i]->node(3), i * 4 + 1 ) );
    pFaceVectorTMP.push_back( new TriangleFace( pTetVectorTMP[i]->node(0),
						pTetVectorTMP[i]->node(2),
						pTetVectorTMP[i]->node(3), i * 4 + 2 ) );
    pFaceVectorTMP.push_back( new TriangleFace( pTetVectorTMP[i]->node(1),
						pTetVectorTMP[i]->node(2),
						pTetVectorTMP[i]->node(3), i * 4 + 3 ) );
//     cout << "3\t"
// 	 << pFaceVectorTMP[ i ]->node( 0 ).id() << "\t"
// 	 << pFaceVectorTMP[ i ]->node( 1 ).id() << "\t"
// 	 << pFaceVectorTMP[ i ]->node( 2 ).id() << endl;
//    dynamic_cast< Tetrahedron*>(pTetVectorTMP[i])->scale( scale );
    dynamic_cast< Tetrahedron*>(pTetVectorTMP[i])->shrink( shrink );
  }

  for (int i = 0; i < nfaces; i ++ ){
    pFaceVectorTMP.push_back( new TriangleFace(*pNodeVectorTMP[ boundary(i).node(0).id() ],
					       *pNodeVectorTMP[ boundary(i).node(1).id() ],
					       *pNodeVectorTMP[ boundary(i).node(2).id() ] ) );
  }

  file << "OFF" << endl;
  file << pNodeVectorTMP.size() << "\t" << pFaceVectorTMP.size() << "\t" << nedges << endl;

  for ( unsigned int i = 0; i < pNodeVectorTMP.size(); i ++ ){
    file << pNodeVectorTMP[i]->x() << "\t" << pNodeVectorTMP[i]->y() << "\t" << pNodeVectorTMP[i]->z() << endl;
  }

  for ( unsigned int i = 0; i < pFaceVectorTMP.size(); i ++ ){
//     cout << i << "\t"<< pFaceVectorTMP[ i ]->id() << endl;
    file << "3\t"
	 << pFaceVectorTMP[ i ]->node( 0 ).id() << "\t"
	 << pFaceVectorTMP[ i ]->node( 1 ).id() << "\t"
	 << pFaceVectorTMP[ i ]->node( 2 ).id() << endl;
  }
  return 1;
}

// BaseMesh Mesh3D::findSubMeshByAttribute( double from, double to ){
//   if ( to == -1 ) to = from;
//   Mesh3D submesh;
//   map < int, int > nodeMap;
//   int count = 0;

//   for ( int i = 0; i < cellCount(); i ++ ){
//     if ( cell( i ).attribute() >= from && cell( i ).attribute() <= to ){
//       for ( int j = 0; j < 4; j ++ ){
// 	if ( nodeMap[ cell( i ).node( j ).id() ] == 0 ){
// 	  count ++;
// 	  nodeMap[ cell( i ).node( j ).id() ] = count;
// 	  submesh.createNode( cell( i ).node( j ).pos(), count - 1, 0 );
// 	}
//       }
//     }
//   }
//   for ( int i = 0; i < cellCount(); i ++ ){
//     if ( cell( i ).attribute() >= from && cell( i ).attribute() <= to ){

//       switch (dim() ){
//       case 2:
// 	submesh.createTriangle( submesh.node( nodeMap[ cell( i ).node( 0 ).id() ] - 1 ),
// 				submesh.node( nodeMap[ cell( i ).node( 1 ).id() ] - 1 ),
// 				submesh.node( nodeMap[ cell( i ).node( 2 ).id() ] - 1 ), i ,
// 				cell( i ).attribute() );
// 	break;
//       case 3:
// 	submesh.createTetrahedron( submesh.node( nodeMap[ cell( i ).node( 0 ).id() ] - 1 ),
// 				   submesh.node( nodeMap[ cell( i ).node( 1 ).id() ] - 1 ),
// 				   submesh.node( nodeMap[ cell( i ).node( 2 ).id() ] - 1 ),
// 				   submesh.node( nodeMap[ cell( i ).node( 3 ).id() ] - 1 ), i ,
// 				   cell( i ).attribute() );
// 	break;
//       }
//     }
//   }
//   return submesh;
// }

void Mesh3D::convertFromMesh2D( const Mesh2D & mesh ){
  clear();
  for ( int i = 0, imax = mesh.nodeCount(); i < imax; i++ ){
    createNode( mesh.node( i ).x(), mesh.node( i ).y(), 0.0,
		nodeCount(),  mesh.node( i ).marker() );
  }
//   for ( int i = 0, imax = mesh.boundaryCount(); i < imax; i++ ){
//     createEdge( node( dynamic_cast< Edge & >( mesh.boundary( i ) ).nodeA().id() ),
// 		node( dynamic_cast< Edge & >( mesh.boundary( i ) ).nodeB().id() ),
// 		boundaryCount(),  mesh.boundary( i ).marker() );
//   }
  for ( int i = 0, imax = mesh.cellCount(); i < imax; i++ ){
    createTriangleFace( node( mesh.cell( i ).node( 0 ).id() ),
			node( mesh.cell( i ).node( 1 ).id() ),
			node( mesh.cell( i ).node( 2 ).id() ),
			cellCount(), mesh.boundary( i ).marker() );
  }
}

} // namespace MyMesh

/*
$Log: mesh3d.cpp,v $
Revision 1.27  2010/10/26 12:20:28  carsten
win32 compatibility commit

Revision 1.26  2009/05/20 12:30:09  carsten
add LOOPTETGEN option in invert script

Revision 1.25  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.24  2007/10/03 20:25:06  carsten
*** empty log message ***

Revision 1.23  2007/09/12 21:31:55  carsten
*** empty log message ***

Revision 1.22  2007/09/11 17:28:58  carsten
*** empty log message ***

Revision 1.21  2006/09/25 11:31:49  carsten
*** empty log message ***

Revision 1.20  2006/09/22 11:05:28  carsten
*** empty log message ***

Revision 1.19  2006/09/21 17:08:40  carsten
*** empty log message ***

Revision 1.18  2006/07/16 20:00:03  carsten
*** empty log message ***

Revision 1.17  2005/10/28 16:09:05  carsten
*** empty log message ***

Revision 1.16  2005/10/28 14:35:55  carsten
*** empty log message ***

Revision 1.15  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.14  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.13  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.12  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.11  2005/05/03 18:46:47  carsten
*** empty log message ***

Revision 1.10  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.9  2005/03/18 19:47:17  carsten
*** empty log message ***

Revision 1.8  2005/03/10 16:04:38  carsten
*** empty log message ***

Revision 1.7  2005/02/18 14:06:19  carsten
*** empty log message ***

Revision 1.6  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.5  2005/02/14 18:58:54  carsten
*** empty log message ***

Revision 1.4  2005/02/10 14:07:59  carsten
*** empty log message ***

Revision 1.3  2005/02/09 15:11:43  carsten
*** empty log message ***

Revision 1.2  2005/01/04 19:44:05  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
