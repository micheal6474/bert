// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "domain2d.h"
#include "spline.h"

Domain2D::Domain2D() : Mesh2D() {
  pFacet_ = new Facet();
}

Domain2D::Domain2D( const string & fileName ) : Mesh2D() {
  pFacet_ = new Facet();
  load( fileName );
}

Domain2D::Domain2D( const Domain2D & domain ) : Mesh2D() {
  pFacet_ = new Facet();
  for ( int i = 0; i < domain.nodeCount(); i ++ ){ createNode( domain.node( i ) ); }
  for ( int i = 0; i < domain.polygonCount(); i ++ ) createPolygon( domain.polygon( i ) );

  for ( int i = 0; i < domain.holeCount(); i ++ ) createHole( domain.hole( i ) );
  for ( int i = 0; i < domain.regionCount(); i ++ ) createRegion( domain.region( i ) );
}

Domain2D::~Domain2D() {
  delete pFacet_;
  for ( uint i = 0; i < vecpHoles_.size(); i ++ ) delete vecpHoles_[ i ];

}

Domain2D & Domain2D::operator = ( const Domain2D & domain ){
  if ( this != & domain ){
    TO_IMPL;
  }
  return *this;
}

void Domain2D::transform( double mat[ 4 ][ 4 ] ){
  BaseMesh::transform( mat );
  for ( int i = 0, imax = regionCount(); i < imax; i ++ ) region( i ).pos().transform( mat );
  for ( int i = 0, imax = holeCount(); i < imax; i ++ ) hole( i ).pos().transform( mat );
}

Polygon * Domain2D::createPolygon( const Polygon & poly ){
  Polygon *p = new Polygon( poly );

  for ( uint i = 0; i < poly.size(); i ++ ){
    (*p)[ i ] = createVIP( poly[ i ]->pos() );
  }
  pFacet_->push_back( *p );
  return p;
}

Polygon * Domain2D::createPolygon( const vector < RealPos > & poly, int marker, bool close ){
  ///*** das ist ziemlich Mist -> aufraeumen

  if ( poly.size() > 1 ){
    Polygon *p = new Polygon( marker );

    vector < Node * > nodeVect;
    for ( uint i = 0; i < poly.size(); i ++ ) {
      nodeVect.push_back( createVIP( poly[ i ].round(1e-14) ) );
    }
    for ( uint i = 0; i < nodeVect.size()-1; i ++ ) createEdge( *nodeVect[ i ], *nodeVect[ i + 1 ], marker );

    if ( close && poly.size() > 2 ) createEdge( *nodeVect.back(), *nodeVect.front(), marker );

    pFacet_->push_back( *p );
    return p;
  }
  return NULL;
}

int Domain2D::load( const string & fileName, int fromone, double snapTol ){
  fstream file; if ( !openInFile( fileName.substr( 0, fileName.rfind( ".poly" ) ) + ".poly", & file ) ) return 0;

  //** read header
  vector < string > row; row = getNonEmptyRow( file );

  if ( row.size() != 4 ) {
    cerr << WHERE_AM_I << " Format unknown! header size = " << row.size() << endl;
    return 0;
  }

  int nVerts = toInt( row[ 0 ] );
  int dimension = toInt( row[ 1 ] );
  if ( dimension != 2 ) {
    cerr << WHERE_AM_I << " Dimension invalid! dimension = " << dimension << endl;
    return 0;
  }

  int nPointsAttributes = toInt( row[ 2 ] );
  bool haveNodeMarker = toInt( row[ 3 ] );

  //** node section
  for ( int i = 0; i < nVerts; i ++ ){
    row = getNonEmptyRow( file );
    if ( (int)row.size() == ( 1 + dimension + nPointsAttributes + haveNodeMarker ) ){
      createNode( RealPos( toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ) );
      if ( haveNodeMarker ){
	node( this->nodeCount() -1 ).setMarker( toInt( row.back() ) );
      }
    } else {
      cerr << WHERE_AM_I << " Poly file corrupt: node section line " << i << " rowsize = " << row.size() << endl;
      return 0;
    }
  }

  //** segment section
  row = getNonEmptyRow( file );
  if ( row.size() != 2 ){
    cerr << WHERE_AM_I << " Format unknown for segment section = " << row.size() << endl;
    return 0;
  }
  int nEdges = toInt( row[ 0 ] );
  int haveBoundaryMarker = toInt( row[ 1 ] );
  int marker = 0;
  for ( int i = 0; i < nEdges; i ++ ){
    row = getNonEmptyRow( file );
    if ( (int)row.size() == ( 3 + haveBoundaryMarker ) ){
      if ( haveBoundaryMarker ) marker = toInt( row[ 3 ] );
      createEdge( this->node( toInt( row[ 1 ] ) ), this->node( toInt( row[ 2 ] ) ), marker );

//       pFacet_->push_back( Polygon( &this->node( toInt( row[ 1 ] ) ), &this->node( toInt( row[ 2 ] ) ) ) );
//       if ( haveBoundaryMarker ) pFacet_->back().setMarker( toInt( row[ 3 ] ) );
    } else {
      cerr << WHERE_AM_I << " Poly file corrupt: segment section line " << i << " rowsize = " << row.size() << endl;
      return 0;
    }
  }

  //** hole section
  row = getNonEmptyRow( file );
  if ( row.size() != 1 ){
    cerr << WHERE_AM_I << " Format unknown for hole section = " << row.size() << endl;
    return 0;
  }
  int nHoles = toInt( row[ 0 ] );
  for ( int i = 0; i < nHoles; i ++ ){
    row = getNonEmptyRow( file );
    if ( row.size() == ( 3 ) ){
      createHole( RealPos( toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ) );
    } else {
      cerr << WHERE_AM_I << " Poly file corrupt: hole section line " << i << " rowsize = " << row.size() << endl;
      return 0;
    }
  }

  //** region section
  row = getNonEmptyRow( file );
  if ( row.size() != 1 ){
    cerr << WHERE_AM_I << " Format unknown for region section = " << row.size() << endl;
    return 0;
  }
  int nRegions = toInt( row[ 0 ] );
  for ( int i = 0; i < nRegions; i ++ ){
    row = getNonEmptyRow( file );
    if ( row.size() == ( 5 ) ){
      createRegion( RealPos( toDouble( row[ 1 ] ), toDouble( row[ 2 ] ) ), toDouble( row[ 3 ] ), toDouble( row[ 4 ] ) );
    } else {
      cerr << WHERE_AM_I << " Poly file corrupt: region section line " << i << " rowsize = " << row.size() << endl;
      return 0;
    }
  }

  file.close();
  return 1;
}

int Domain2D::save( const string & fileName, int fromone ){

  fstream file; if ( !openOutFile( fileName.substr( 0, fileName.rfind( ".poly" ) ) + ".poly", & file ) ) return 0;

  int nVerts = nodeCount();

  //  file << "# nverties dimension nattrib boolbndrymarker" << endl;
  file << nVerts << "\t2\t0\t1" << endl;

  //file << "#    pointNr. x y z marker" << endl;
  file.setf( ios::scientific, ios::floatfield );

  file.precision( 14 );
  for ( int i = 0; i < nVerts; i++ ){
    file << i
	 << "\t" << node( i ).x()
	 << "\t" << node( i ).y()
	 << "\t" << node( i ).marker() << endl;
  }

  //file << "# nfacets boolbndrymarker" << endl;
  size_t nPolygons = pFacet_->size();
  size_t nNodesPoly = 0;
  size_t nEdges = this->countEdges();

  file << nEdges << "\t1" << endl;

  size_t edgeCounter = 0;
  for ( size_t i = 0; i < nPolygons; i ++ ){
    nNodesPoly = (*pFacet_)[ i ].size();
    if ( nNodesPoly > 1 ) {
      if ( (*pFacet_)[ i ][ 0 ]->id() != (*pFacet_)[ i ][ 1 ]->id() ){
	for ( size_t k = 0, kmax = nNodesPoly -1; k < kmax; k++ ){
	  file << edgeCounter << "\t" << (*pFacet_)[ i ][ k ]->id() << "\t"
	       << (*pFacet_)[ i ][ k + 1 ]->id() << "\t" << (*pFacet_)[ i ].marker() << endl;
	  edgeCounter ++;
	}
      }
    }
  }
  //file << "# nholes" << endl;
  file << vecpHoles_.size() << endl;
  //file << "#    hole x y z" << endl;
  for ( int i = 0, imax = vecpHoles_.size(); i < imax ; i ++ ){
    file << i << "\t"
	 << vecpHoles_[ i ]->x() << "\t"
	 << vecpHoles_[ i ]->y() << endl;
  }

  //file << "# nregions" << endl;
  file << regions_.size() << endl;
  //file << "#    region x y z attribute maxarea" << endl;
  for ( int i = 0, imax = regions_.size(); i < imax ; i ++ ){
    file << i << "\t"
 	 << regions_[ i ]->x() << "\t"
 	 << regions_[ i ]->y() << "\t"
 	 << regions_[ i ]->attribute() << "\t"
 	 << regions_[ i ]->dx() << endl;
  }
  file.close();

  return 1;
}

void Domain2D::merge( const Domain2D & subdomain, double tol,  bool check ){
  //if ( check )
  link( subdomain );
  //else add( subdomain );

}

void Domain2D::link( const Domain2D & subdomain ){
  Node * tmpNode;
  size_t oldNodeCount = nodeCount();

  //** copy nodes, with duplicate check;

  for ( int i = 0, imax = subdomain.nodeCount(); i < imax; i ++ ){
    tmpNode = this->createVIP( subdomain.node( i ).pos(), subdomain.node( i ).marker() );
    subdomain.node( i ).setId( tmpNode->id() );
  }

  for ( int i = 0, imax = subdomain.polygonCount(); i < imax; i ++ ){
    for ( int j = 0, jmax = subdomain.polygon( i ).size()-1; j < jmax; j ++ ){
      this->createEdge( this->node( subdomain.polygon( i )[ j ]->id() ),
			this->node( subdomain.polygon( i )[ j+1 ]->id() ),
			subdomain.polygon( i ).marker() );
    }
  }

  for ( int i = 0, imax = subdomain.regionCount(); i < imax; i ++ ) insertRegionMarker( subdomain.region( i ) );
  for ( int i = 0, imax = subdomain.holeCount(); i < imax; i ++ ) createHole( subdomain.hole( i ) );
}

int Domain2D::countEdges() const {
  int nNodesPoly = 0;
  int nEdges = 0;
  for ( size_t i = 0; i < pFacet_->size(); i ++ ){
    nNodesPoly = (*pFacet_)[ i ].size();
    if ( nNodesPoly > 1 ){
      if ( (*pFacet_)[ i ][ 0 ]->id() != (*pFacet_)[ i ][ 1 ]->id() ){
	nEdges += (nNodesPoly-1);
      }
    }
  }
  return nEdges;
}

void Domain2D::createSimpleMainDomain( const RealPos & start, const RealPos & end ){

 Node *n1 = createNode( start, nodeCount(), 1 );
 Node *n2 = createNode( end.x(), 0.0, nodeCount(), 2 );
 Node *n3 = createNode( end.x(), end.y(), nodeCount(), 3 );
 Node *n4 = createNode( start.x(), end.y(), nodeCount(), 4 );

 Polygon surface( HOMOGEN_NEUMANN_BC );
 surface.push_back( n1 );
 surface.push_back( n2 );

 Polygon subSurface( MIXED_BC );
 subSurface.push_back( n2 );
 subSurface.push_back( n3 );
 subSurface.push_back( n4 );
 subSurface.push_back( n1 );

 pFacet_->push_back( surface );
 pFacet_->push_back( subSurface );
}

void Domain2D::createInterface( const vector < RealPos > & verts, double dx, bool equidistDX,
                                int marker, bool spline, bool close, int vertsMarker  ){
  if ( dx > 1.0/3.0 ) dx = 0.5;
  if ( spline ) {
    if ( equidistDX ){
      this->createPolygon( createSpline( verts, (int)rint(1.0 / dx), close ), marker, close );
    } else {
      this->createPolygon( createSplineLocalDX( verts, dx, close ), marker, close );
    }
  } else { // nospline
    vector< Node * > boundaryNodes;
    RealPos next;
    for ( uint i = 0; i < verts.size(); i ++ ) {

      if ( !close && i == verts.size()-1 ) break;
      boundaryNodes.push_back( this->createVIP( verts[ i ] ) );

      if ( i == verts.size() -1 ) next = verts[ 0 ]; else next = verts[ i + 1 ];

      if ( dx > 0 ){
	if ( equidistDX ){
	  for ( int j = 0; j < rint( 1.0 / dx)-1; j ++ ){
	    boundaryNodes.push_back( this->createVIP( Line( verts[ i ], next ).lineAt( (double)(j+1) * 1.0 / rint( 1.0 / dx) ) ) );
	  }
	} else {
	  boundaryNodes.push_back( this->createVIP( Line( verts[ i ], next ).lineAt( dx ) ) );
	  if ( dx <= 1.0/3.0 ) boundaryNodes.push_back( this->createNode( Line( verts[ i ], next ).lineAt( 1-dx ) ) );
	}
      }
    }

    if ( !close ) boundaryNodes.push_back( this->createVIP( verts.back() ) );
    for ( size_t i = 0; i < boundaryNodes.size() - 1; i++ ){ // for all surface nodes
      this->createEdge( *boundaryNodes[ i ], *boundaryNodes[ i + 1 ], marker );
    }
    if ( close ) this->createEdge( *boundaryNodes.back(), *boundaryNodes[ 0 ], marker );
  }

  for ( uint i = 0; i < verts.size(); i ++ ) this->createVIP( verts[ i ], vertsMarker );
}

void Domain2D::createCircleMainDomain( const vector < RealPos > & verts, double dx, bool equidistDX, double area,
				       int marker, bool spline, int vertsMarker ){
  if ( dx > 1.0/3.0 ) dx = 0.5;

  if ( spline ) {
    if ( equidistDX ){
      this->createPolygon( createSpline( verts, (int)rint(1.0 / dx), true ), HOMOGEN_NEUMANN_BC, true );
    } else {
      this->createPolygon( createSplineLocalDX( verts, dx, true ), HOMOGEN_NEUMANN_BC, true );
    }
  } else { // nospline
    vector< Node * > boundaryNodes;
    RealPos next;
    for ( uint i = 0; i < verts.size(); i ++ ) {
      boundaryNodes.push_back( this->createNode( verts[ i ] ) );
      if ( i == verts.size() - 1 ) next = verts[ 0 ]; else next = verts[ i + 1 ];

      if ( dx > 0 ){
	if ( equidistDX ){
	  for ( int j = 0; j < rint( 1.0 / dx)-1; j ++ ){
	    boundaryNodes.push_back( this->createNode( Line( verts[ i ], next ).lineAt( (double)(j+1) * 1.0 / rint( 1.0 / dx) ) ) );
	  }
	} else {
	  boundaryNodes.push_back( this->createNode( Line( verts[ i ], next ).lineAt( dx ) ) );
	  if ( dx <= 1.0/3.0 ) boundaryNodes.push_back( this->createNode( Line( verts[ i ], next ).lineAt( 1-dx ) ) );
	}
      }
    }
    for ( size_t i = 0; i < boundaryNodes.size() - 1; i++ ){ // for all surface nodes
      this->createEdge( *boundaryNodes[ i ], *boundaryNodes[ i + 1 ], HOMOGEN_NEUMANN_BC );
    }
    this->createEdge( *boundaryNodes.back(), *boundaryNodes[ 0 ], HOMOGEN_NEUMANN_BC );
  }

  for ( uint i = 0; i < verts.size(); i ++ ) this->createVIP( verts[ i ], vertsMarker );

  RealPos center( averagePosition( verts ) );

  Mesh2D paraMesh = this->createMesh( 0.0 );

  this->createRegionMarker( center, marker, paraMesh.area() * area ); // 2 for parametric domain
  this->createVIP( center, -999 );

}

void Domain2D::refineElectrodes( const RealPos & dxyz, double dr ){

  vector < int > sources( this->getAllMarkedNodes( -99 ) );
  double dxdist = 9e99;

  RealPos center( averagePosition( this->nodePositions() ) );
  //  cout << WHERE_AM_I << center << endl;  this->createVIP( center );

  double spacing = 1.0;

  for ( uint i = 0; i < sources.size(); i ++ ){
    if ( fabs(dr) > 0.0 ) {
      if ( i < sources.size()-2) spacing = this->node( sources[ i ] ).distance( this->node( sources[ i+1 ] ) );
      this->createVIP( Line( this->node( sources[ i ] ).pos(), center ).lineAt(
	       (spacing*dr)/this->node( sources[ i ] ).pos().distance( center ) ) );
    } else {
      this->createVIP( this->node( sources[ i ] ).pos() + dxyz );
    }
    dxdist = min( dxdist, this->node( sources[ i ] ).distance( this->node( this->nodeCount() -1 ).pos() ) );
  }

  sources = this->getAllMarkedNodes( -999 );
  for ( uint i = 0; i < sources.size(); i ++ ){
    this->createNode( this->node( sources[ i ] ).pos() + RealPos( dxdist, 0.0, 0.0 ) );
  }
}

Domain2D create2DWorld( const RealPos & size, int marker, double area ){
  Domain2D world;

  world.createSimpleMainDomain( RealPos( -size.x() / 2.0, 0.0),
				RealPos( size.x() / 2.0,  -size.y() ) );
  world.insertRegionMarker( RealPos( size.x() / 2.0, 0.0) - size/1000.0, marker, area );

  return world;
}

void Domain2D::insertNode( Node * node ){
  pFacet_->insertNode( node );
}

Node * Domain2D::nodeAt( const RealPos & pos, double snapTol ){

  for ( int i = 0, imax = nodeCount(); i < imax; i ++ ){
    if ( node( i ).pos().distance( pos ) < snapTol ){
      return &node( i );
    }
  }
  return NULL;
}

Node * Domain2D::createVIP( const RealPos & pos, int marker, double snapTol ){
  Node * node;
  if ( (node = nodeAt( pos, snapTol ) ) == NULL ){
    node = Mesh2D::createNode( pos, nodeCount(), marker );
    //    cout << WHERE_AM_I << node->id() << node->pos() << endl;
    insertNode( node );
  } else{
    if ( node->marker() > -99 ) node->setMarker( marker );
  }
  return node;
}

void Domain2D::createLineOfVIPs( const RealPos & start, const RealPos & end, int nVerts, int marker ){
  if  ( nVerts < 2 ) cerr << WHERE_AM_I << " nVerts < 2" << endl;
  Line line( start, end );

  double dt = 1.0 / ( nVerts - 1 );
  for ( int i = 0; i < nVerts; i ++ ){
    createVIP( line.lineAt( dt * i ),  marker );
  }
}

vector < RealPos > Domain2D::createPolygonInterpolate( const vector < RealPos > & poly,
						       const RealPos & start,
						       double spacing, int nVerts, int direction, bool close ){

  TO_IMPL


return vector < RealPos >();
                                   
                            }

void Domain2D::createLineOfVIPsSurface( const RealPos & start, double spacing, int nVerts, int marker,
					int surfaceMarker, int direction ){

  //** delete all new Nodes oder mach das irgendwie schoener

  //** sammle alle Knoten auf Polygon mit surfaceMarker
  Domain2D tmpDomain;
  for ( int i = 0; i < this->polygonCount(); i ++ ){
    if ( this->polygon( i ).marker() == surfaceMarker ){
      for ( uint j = 0; j < polygon( i ).size(); j ++ ){
	tmpDomain.createVIP( polygon( i )[ j ]->pos() );
      }
    }
  }
  //** alle knoten in das topo polygon, in der Hoffnung die sind nach x aufsteigend sortiert. :(
  vector < RealPos > poly( tmpDomain.nodePositions( ) ), tmpVerts, newVerts;

  Line line;
  int firstEdgeNodeIdx = -1;
  RealPos startVIP( start );

  //** checke ob start auf einer Edge der Surface liegt;
  for ( uint i = 0; i < poly.size() - 2; i ++ ){
    int touch = Line( poly[ i ], poly[ i + 1 ] ).touch( start );
    if ( touch == 2 || touch == 3 ){
      firstEdgeNodeIdx = i;
      startVIP = start;
      break;
    }
  }

  if ( firstEdgeNodeIdx == -1 ){ //** wenn nicht wird er in y-Richtung dahin verschoben;
    for ( uint i = 0; i < poly.size() - 2; i ++ ){
      if ( start.x() > min( poly[ i ].x(), poly[ i + 1 ].x() ) &&
	   start.x() < max( poly[ i ].x(), poly[ i + 1 ].x() ) ){
	line = Line( poly[ i ], poly[ i + 1 ] );
	startVIP = line.intersect( Line( start, start - RealPos( 0.0, 10.0 ) ) );
	firstEdgeNodeIdx = i;
	break;
      }
    }
  }
  if ( firstEdgeNodeIdx == -1 ){ //** Wir haben ein Problem: kein startpunkt gefunden.
    cerr << WHERE_AM_I << " no startposition found." << endl;
  }

  tmpVerts.push_back( startVIP ), newVerts.push_back( startVIP );
  RealPos lastPos;
  Edge *edge = new Edge( *new Node( poly[ firstEdgeNodeIdx%poly.size() ] ),
			 *new Node( poly[ (firstEdgeNodeIdx + direction)%poly.size() ] ) );
  int activeEdge = firstEdgeNodeIdx;
  double needLength = 0.0, nextLength = 0.0;
  bool already = false;
  for ( int i = 1; i < nVerts; i ++ ){

    already = false;
    lastPos = tmpVerts.back( );

    line = Line( lastPos, edge->node( 1 ).pos() );
    nextLength = line.length();
    needLength = spacing;

//      cout << nextLength << " " <<     needLength << endl;
//      cout << edge->node( 0 ).pos() << edge->node( 1 ).pos() << endl;

    while ( (needLength -= nextLength) >= 0 ) {

      if ( needLength == 0.0 ) {
 	already = true;
 	newVerts.push_back( edge->node( 1 ).pos() );
	tmpVerts.push_back( edge->node( 1 ).pos() );
      } else tmpVerts.push_back( edge->node( 1 ).pos() );

      activeEdge += direction;
//       if ( activeEdge > poly.size() -2){
// 	cerr << WHERE_AM_I << " to few topo infos: " << endl;
// 	return;
//       }

      edge = new Edge( * new Node( poly[ activeEdge%poly.size() ] ),
		       * new Node( poly[ (activeEdge + direction)%poly.size() ] ) );

      lastPos = tmpVerts.back();
      line = Line( lastPos, edge->node( 1 ).pos() );
      nextLength = line.length();
      if ( needLength < nextLength ){
	break;
      }
    }
    if ( needLength <= 0.0) needLength = spacing;
    if ( !already ) {
      newVerts.push_back( line.lineAt( needLength / line.length() ) );
      tmpVerts.push_back( newVerts.back() );
    }
  }

  for ( uint i = 0; i < newVerts.size();i ++ ){
    //    cout << i << newVerts[ i ] << endl;
    this->createVIP( newVerts[ i ], marker );
  }
}

int Domain2D::createMesh( Mesh2D & mesh, double quality, double area, bool verbose ){
  mesh.clear();
  char strdummy[128];
  //** das grosse  A scheint ignoriert zu werden, wenn a mit argument uebergeben dann werden die region-a igoriert
    //** triangle 1.6;
  if ( area == 0.0 ){
    if ( verbose ) { sprintf(strdummy, "-pq%fzeAfa", quality );
    } else { sprintf(strdummy, "-Qpq%fzeAfa", quality ); }
  } else {
    if ( verbose ) { sprintf(strdummy, "-pq%fzeAfa%f", quality, area );
    } else { sprintf(strdummy, "-Qpq%fzeAfa%f", quality, area ); }
  }
  //  cout << WHERE_AM_I << strdummy<< endl;
  TriangleWrapper triangle( *this, mesh, strdummy );
return 1;
    
}

/*
$Log: domain2d.cpp,v $
Revision 1.31  2010/01/31 10:49:54  carsten
*** empty log message ***

Revision 1.30  2009/04/21 21:19:57  carsten
*** empty log message ***

Revision 1.29  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.28  2008/04/15 16:53:04  carsten
*** empty log message ***

Revision 1.27  2007/05/22 18:50:04  carsten
*** empty log message ***

Revision 1.26  2007/04/20 12:11:26  carsten
*** empty log message ***

Revision 1.25  2006/11/08 19:53:24  carsten
*** empty log message ***

Revision 1.24  2006/08/21 15:21:12  carsten
*** empty log message ***

Revision 1.23  2006/07/24 16:24:14  carsten
*** empty log message ***

Revision 1.22  2006/07/16 20:00:03  carsten
*** empty log message ***

Revision 1.21  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.20  2006/04/19 15:35:19  carsten
*** empty log message ***

Revision 1.19  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.18  2006/03/13 18:33:49  carsten
*** empty log message ***

Revision 1.17  2006/03/02 23:45:31  carsten
*** empty log message ***

Revision 1.16  2006/03/02 18:30:20  carsten
*** empty log message ***

Revision 1.15  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.14  2005/12/20 13:41:40  carsten
*** empty log message ***

Revision 1.13  2005/12/06 13:48:07  carsten
*** empty log message ***

Revision 1.12  2005/10/30 18:53:04  carsten
*** empty log message ***

Revision 1.11  2005/10/20 14:33:49  carsten
*** empty log message ***

Revision 1.10  2005/10/18 19:18:13  carsten
*** empty log message ***

Revision 1.9  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.8  2005/09/12 18:50:50  carsten
*** empty log message ***

Revision 1.7  2005/09/12 10:38:36  carsten
*** empty log message ***

Revision 1.6  2005/09/08 19:08:48  carsten
*** empty log message ***

Revision 1.5  2005/09/08 11:25:15  carsten
*** empty log message ***

Revision 1.4  2005/08/30 21:41:17  carsten
*** empty log message ***

Revision 1.3  2005/08/30 15:43:28  carsten
*** empty log message ***

Revision 1.2  2005/08/24 20:22:11  carsten
*** empty log message ***

Revision 1.1  2005/07/28 19:43:43  carsten
*** empty log message ***

*/
