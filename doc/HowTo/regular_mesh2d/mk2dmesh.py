# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 20:01:31 2011

@author: Guenther.T
"""

import pygimli as pg
import numpy as np
from pygimli.meshtools import appendTriangleBoundary

data = pg.DataContainer('gallery.dat')
xmin = data.sensorPosition(0)[0]
xmax = data.sensorPosition(data.sensorCount() - 1)[0]
dx = (data.sensorPosition(1)[0] - xmin) / 2.
zmax = 10

nb = 2
x = pg.asvector(np.arange(xmin-dx*nb, xmax+dx*(nb+1), dx))
z = pg.asvector(np.arange(np.ceil(-zmax / dx), 1.) * dx)
print x, z

mesh = pg.Mesh()
mesh.create2DGrid(x, z)
for c in mesh.cells():
    c.setMarker(2)

mesh2 = appendTriangleBoundary(mesh, 50., 50.)
pg.show(mesh2)
mesh2.save('mesh.bms')
