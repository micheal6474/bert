#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 02 15:39:13 2012

@author: Guenther.T and Ruecker.C
"""

import pygimli as pg
import pybert as pb

import numpy as np

import os 

"""
    prepare directories for 2d files and positions
"""
for d in ['2d', '3d', 'pos']:
    if not os.path.exists(d):
        os.mkdir(d)

"""
    First we read in the filenames and points out of the profile 
    load profile (*.pro) file
"""
profileFileName = 'alldata.pro'
with open(profileFileName, 'r') as fi:
    content = fi.readlines()
fi.close()
    
XL, YL, fNames = [], [], []
for line in content:    
    fNames.append(line.split()[0])
    XL.append(np.double(line.split()[1::2]))
    YL.append(np.double(line.split()[2::2]))

"""
    Next, we read in the DEM and make a Delaunay triangulation
"""
x, y, z = np.loadtxt('topo.xyz', unpack=True)
mesh = pg.meshtools.createMesh(zip(x,y))

"""
    The resulting Delaunay mesh have the same amount of nodes as the number of DEM points
    So we can view the resulting topography directly
"""
ax, cbar = pg.show(mesh, data=z, showLater=1)
pg.show(mesh, axes=ax, showLater=1)
ax.grid(True)

"""
    Optionally we can export a vtk, so we need to set the height into the 2d mesh
"""
for n in mesh.nodes():
    n.setPos([n.pos()[0], n.pos()[1], z[n.id()]])
mesh.exportVTK('topo.vtk')

"""
    For each of the profiles we need to roll-on the tape coordinates from the 
    datafile on the topography. Therefore we create a densely sampled point
    list along the profile course (given by xl/yl arrays) and interpolate
    the z-coordinate 
"""
acc = 0.01 # distance between two interpolation points
data3d = pb.DataContainerERT() # empty 3d data to be filled with 2d data

for xl, yl, datfile in zip(XL,YL,fNames):
    data = pb.DataContainerERT(datfile)
    profileCoords = pg.x(data.sensorPositions())
    
    le = np.hstack((0., np.cumsum(np.sqrt(np.diff(xl)**2 + np.diff(yl)**2))))
    
    xi = np.interp(np.arange(le[0], le[-1] + acc, acc), le, xl)
    yi = np.interp(np.arange(le[0], le[-1] + acc, acc), le, yl)
    zi = pg.interpolate(mesh, z, np.vstack([xi, yi]).T)
    
    """
        For the obtained point list we compute the tape distance along the 
        topography and interpolate all three coordinates from it. 
    """

    dt = np.sqrt(np.diff(xi)**2 + np.diff(yi)**2 + np.diff(zi)**2)
    ti = np.hstack((0., np.cumsum(dt)))
    ze = np.interp(profileCoords, ti, zi)
    xe = np.interp(profileCoords, ti, xi)
    ye = np.interp(profileCoords, ti, yi)
    ax.plot(xe, ye, '.')

    """
        For a 2d inversion we need the direction along the profile. 
        The tape-true coordinate mapping is saved to some temporary file 
        that will later be used.     
    """

    x2d = np.hstack((0., np.cumsum(np.sqrt(np.diff(xe)**2 + np.diff(ye)**2))))
    allCoords = np.vstack((profileCoords, xe, ye, ze, x2d))
    posfile = 'pos/' + datfile.replace('.dat','.txyz')
    np.savetxt('pos/' + datfile.replace('.dat','.txyz'), allCoords.T)

    """
        First we want to generate a valid 2d file using the latter coordinate, 
        saving exactly the field read in:
    """

    for i in range(data.sensorCount()):
        po = pg.RVector3(round(x2d[i]/acc)*acc, 0., round(ze[i]/acc)*acc)
        data.setSensorPosition(i, po)        

    data.save('2d/' + datfile, data.inputFormatString())

    """
        Next, we set the coordinates to the real position and subsequently add 
        the data to a previously created empty data container.
    """
    for i in range(data.sensorCount()):
        po = pg.RVector3(round(xe[i]/acc)*acc, round(ye[i]/acc)*acc, round(ze[i]/acc)*acc)
        data.setSensorPosition(i, po)

    data3d.add(data, acc) 


data3d.save('3d/' + profileFileName.replace('.pro','.dat'), data.inputFormatString())

"""
    We are now ready for doing the inversion of the individual 2d files and the 3d file.
"""

pg.showNow()