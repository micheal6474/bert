#!/usr/bin/env python

import os
import pygimli as pg
import numpy as np
from pygimli.meshtools import appendTriangleBoundary

data = pg.DataContainer('vest-ost.dat')

z1 = data.sensorPosition(0)[2]  # z of first electrode
z2 = data.sensorPosition(1)[2]  # z of second electrode
zN = data.sensorPosition(data.sensorCount() - 1)[2]
dz = z2 - z1  # regular spacing with electrode distance
nb = 2        # number of boundary elements
nx2 = 20       # half number of x cells

# position vectors, regular spacing
xx = np.arange(-nx2, nx2) * dz
zz = np.arange(z1 - nb * dz, zN + nb * dz, dz)
mesh = pg.createMesh2D(xx, zz, 2)

# append triangles around mesh, show and save it
mesh2 = appendTriangleBoundary(mesh, 70., 70., marker=0, isSubSurface=True)
print(mesh)
print(mesh2)
pg.show(mesh2, data=mesh2.cellMarkers(), linear=True)

os.makedirs('mesh', exist_ok=True)
mesh2.save('mesh/mesh.bms')
#pg.wait()
