# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 17:12:59 2011

@author: Guenther.T
"""

import pygimli as g
# read inner and outer mesh
Outer = g.Mesh('right.bms')
Inner = g.Mesh('para.bms')
print(Outer)
print(Inner)
# set all outer cells to 1
for c in Outer.cells():
    c.setMarker(1)
# set all inner cells to 2
for c in Inner.cells():
    c.setMarker(2)
    Outer.copyCell(c)

print(Outer)
Outer.save('mesh')
Outer.exportVTK('mesh')
