# -*- coding: utf-8 -*-
"""
Created on Tue May 31 18:42:37 2011

@author: Guenther.T
"""

import pygimli as pg
import numpy as np

mesh = pg.Mesh()
mesh.importVTK('dcinv.result.vtk')

tn = [n.pos()[0] for n in mesh.nodes()]
zn = [n.pos()[1] for n in mesh.nodes()]

A = np.loadtxt('pos.map').T

xn = np.interp(tn, A[0], A[1])
yn = np.interp(tn, A[0], A[2])

for i, n in enumerate(mesh.nodes()):
    n.setPos(pg.RVector3(xn[i], yn[i], zn[i]))

mesh.exportVTK('result3d.vtk')
