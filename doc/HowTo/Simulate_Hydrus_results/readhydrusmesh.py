# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 07:33:50 2011

@author: Guenther.T
"""

import pygimli as pg

mesh = pg.Mesh()
f = open('MESHTRIA.TXT', 'r')
for i in range(6):
    line1 = f.readline()

nnodes = int(line1.split()[0])
ncells = int(line1.split()[1])
print nnodes, ncells
line1 = f.readline()
nodes = []
dx = 0.01
for ni in range(nnodes):
    pos = f.readline().split()
    p = pg.RVector3(float(pos[1])*dx, float(pos[2])*dx, float(pos[3])*dx*(-1.))
    n = mesh.createNode(p)
    nodes.append(n)

line1 = f.readline()
line1 = f.readline()
cells = []
for ci in range(ncells):
    pos = f.readline().split()
    i, j, k, l = int(pos[1]), int(pos[2]), int(pos[3]), int(pos[4])
    c = mesh.createTetrahedron(nodes[i-1], nodes[j-1], nodes[k-1], nodes[l-1])
    cells.append(c)

f.close()

print(mesh)
mesh.save('hydrus')
mesh.exportVTK('hydrus')

mesh.createNeighbourInfos()
for b in mesh.boundaries():
    if not (b.leftCell() and b.rightCell()):
        b.setMarker(1)

poly = pg.Mesh()
poly.createMeshByBoundaries(mesh, mesh.findBoundaryByMarker(1))
poly.exportAsTetgenPolyFile('paraBoundary')
