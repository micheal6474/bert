How to use 2d with original topography

File: doc/HowTo/2d_topofile
Task: use unidata format file with topography at end for inversion
Problem: dataContainer does not support this yet
Solution: data have to be split and transformed to standard format
(Should be part of BERT2 very soon)

We solve the problem in a shell script and invoke Python directly.
First we extract the number of electrodes and data and write the first
parts (electrodes+data) of the file such that be read by pygimli.
Moreover we read the topography list and export it to another file:

infile='profile-topo.dat'
datfile='profile.dat'
outfile='profile.ohm'
topofile='topo.xz'
read line < $infile
nel=${line%#*}
line=`head -n $[nel + 3] $infile|tail -n 1`
ndata=${line%#*}
head -n $[nel + $ndata + 4] $infile > $datfile
line=`head -n $[nel + $ndata + 5] $infile|tail -n 1`
ntopo=${line%#*}
echo $nel $ndata $ntopo
tail -n $ntopo $infile > $topofile

We now switch to python and read in the file and extract electrode
positions. We compute the tape distance along the profile and use
the topography to interpolate it onto the electrode positions.

data=g.DataContainer('$datfile')
print data
A  = N.loadtxt('$topofile').T
tt = N.hstack( (0., N.cumsum( N.sqrt( N.diff(A[0])**2 + N.diff(A[1])**2 ) ) ) )
xe = [pos[0] for pos in data.sensorPositions()]
z  = N.interp( xe, A[0], A[1] )

However, usually the electrode positions have tape distances and
not real x coordinates. Therefore we have to account for their heights
such that the tape distance remains constant.
After it we set 

x  = xe[0] + N.hstack( (0., N.cumsum( N.sqrt( N.diff(xe)**2 - N.diff(z)**2 ) ) ) )
de = N.sqrt( N.diff(x)**2 + N.diff(z)**2 )
for i in range( data.sensorCount() ):
    data.setSensorPosition( i, g.RVector3( x[i], 0.0, z[i] ) )
    
data.save('$outfile','a b m n u i R','x z')
END
