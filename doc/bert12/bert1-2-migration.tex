\documentclass[a4paper]{scrartcl}
\usepackage{hyperref}
\usepackage{graphicx} % PDF
\usepackage{xspace}
\usepackage{hyperref}
\usepackage{natbib}
\graphicspath{{pics/}}
\pagestyle{empty}
\parindent 0pt
\newcommand{\GIMLi}{\textsc{GIMLi}}
\newcommand{\dcfemlib}{\textsc{dcfemlib}}
\begin{document}

\title{Migration guide from BERT version 1 to 2}
\author{
Thomas G{\"u}nther\thanks{Leibniz Institute for Applied Geophysics, Hannover} 
\ \& \ Carsten R{\"u}cker\thanks{Institute of Geology and Geophysics, University of Leipzig}
}

\maketitle

\section{Introduction}
This document has been written at the time when BERT 2 does not yet have a fully covered tutorial.
If you read this content now, it should all be integrated, so better look into the BERT tutorial of version 2.1 or higher.
It helps people that already worked with BERT 1 to learn about what BERT 2 can do additionally or better.

BERT in the versions 0.x-1.x is fully based on \dcfemlib, a library for finite element based forward and inverse calculations of direct current problems including mesh generation tools.
The library was completely re-written in order to apply for physics-independent modelling and inversion with a very general approach.
\GIMLi\ - the \textbf{G}eophysical \textbf{I}nversion and \textbf{M}odelling \textbf{Li}brary\footnote{See project website \url{gimli.org} or github project website \url{https://github.com/gimli-org/gimli}} is a multi-method modelling and inversion library
It holds some forward operators such as 1d em or travel time problems.
The direct current part is not distributed along with \GIMLi, but requires the libbert library for compilation.

As a result, a completely new-written ERT code was created which is still BERT (the same technology) and is defining the second generation, referred to as BERT 2.
Although the numerical techniques are mainly the same, there were numerous improvements.
Main advances of version 2 over 1 are
\begin{itemize}
	\item more flexible treatment of different regions (different behaviour, constraints and transform functions) , inter-region (de)coupling and as a result of this more possibilities to include prior knowledge (section \ref{sec:newinv})
	\item more constraint types (zeroth, first and second order plus combinations of them) and a stabler way of anisotropic regularization
	\item more element types such as rectangular, hexahedral, or prism meshes or a combination of different elements (section \ref{sec:newmesh})
	\item use of different electrode types: nodes, surfaces (CEM electrodes) or node-free points (or a mix of different types)
	\item easy data filtering using different MIN/MAX keywords (section \ref{sec:newdata})
	\item lower/upper resistivity bounds can be applied independent of the data (in BERT1 the transformation was also applied to the $\rho_a$ values)
	\item improved time-lapse strategies, e.g. full reference model minimization, difference inversion (see \ref{sec:newtl})
	\item improved direct visualization using pyGIMLi (Python bindings of \GIMLi) (section \ref{sec:newplot})
	\item improved speed (distributed computing) or convergence and easier build procedure
	\item improved phase inversion due to more rigorous IP scheme
\end{itemize}

However, the general style of usage has not been changed, i.e.:
\begin{itemize}
	\item a config file is holding options/settings (see section \ref{sec:newoptions} for changes)
	\item new option files with default settings and useful alternatives are produced using bertNew2d, bertNew2dTopo, bertNew3d, bertNew3dTopo, bertNew2dCirc and bertNew3dCyl
	\item the actions are called using the command bert (formerly invert) with the cfg file and a list of actions, see section \ref{sec:commands} for the changes
\end{itemize}

BERT 2 is expected to be backward compatible, i.e. old cfg files can be used.
The results are expected to be similar but not identical since a more rigorous minimization scheme is used and some details are changed that can affect the convergence (in both directions).

As an essential part, the mesh generation using \dcfemlib-based programs is still the same, but will be replaced completely by Python tools in future versions (probably milestone 2.5 or 3.0).
The visualization is done as usual with ParaView (recommended for 3D), Matlab (see GIMLi::matlab and BERT::matlab toolboxes) or Python (default for 2D).
The whole package is moving more and more to Python, possible the applications will by py-files only.
Currently, Python is not strictly needed to run BERT, but it is advisable for easy 2d plots of mesh and inversion results\footnote{The numerics library Numpy and the Plotting library matplotlib are needed. We recommend the distribution WinPython that brings along many useful packages for science.}.
%However, as soon the software becomes independent from the BERT1 mesh and poly tools, Python will be required.
%Python is also a nice way to write graphical user interfaces (GUI's).
%There is already a couple of GUIs for specific tasks newly created or being ported to python: CylBERT, TreeBERT, BERT2D.

BERT 2 can be installed using a binary installer (available for Windows 32 or 64 bit, conda packages for Linux) or installed build using the source code.
The installers bring along the needed executables, pyGIMLi binaries and modules, pyBERT binaries and modules, and the whole BERT 1 executables, so that it can also be run with the version 1.3.

\clearpage
\section{Changes on a glance}\label{sec:changes}
\subsection{Commands}\label{sec:commands}
Most of the commands such as all, show, calc remain.

{\bf primPot/interpolate $\Rightarrow$ pot}\\
Formerly the primary potential was computed and interpolated in two steps.
Now the potentials are interpolated onto the forward mesh on-the-fly unless otherwise stated by KEEPPRIMPOT=1.

%interpolate only for KEEPPRIMPOT
{\bf correct $\Rightarrow$ filter}\\
As the command correct (called before calc) was leading to confusions, the command filter now transforms the raw data into the desired inversion input including apparent resistivity, geometric factor and error.
Additionally filtering is done using MIN/MAX keywords.
For timelapse problems filter will homogenize the data array for the individual time steps.
In future versions an automated normal reciprocal analysis can be called if the data allow for that.

{\bf calc/calcSensM $\Rightarrow$ calc}\\
The calcSensM command is not needed any more and fully replaced by calc.
Recalculation of the Jacobian is the default.
%The matrix is only to loaded from disk (e.g. for large 3d problems) if present and valid.

New commands are:

sensOnly - calculate only the Jacobian matrix (for sensitivity or resolution studies) 

\textbf{version} - prints out the version 

\textbf{mkpdf} - autogenerates a result pdf (in case of 2d geometry)

\textbf{showmesh} - show only the (parameter) mesh (for optimizing mesh options)

\textbf{mkmeshpdf} - make a pdf file of the parameter mesh



\subsection{New options/variables in the config file}\label{sec:newoptions}
\subsubsection{General behaviour and data}\label{sec:newdata}
\begin{description}
	\item[BERTTHREADS] number of threads (computer cores) for distributed computing
	\item[SAVEALOT] - debug mode for additional output and files
	\item[RMIN, RMAX, KMIN, KMAX, ERRMAX, IPMIN, IPMAX] - data filtering options for valid ranges of apparent resistivity, geometry factor, error and IP
	\item[DOWNWEIGHT] - invalid data are downweighted instead of deleted
\end{description}

\subsubsection{Inversion and regularization}\label{sec:newinv}
\begin{description}
	\item[REGIONFILE] - the most powerful option holding a filename (see section \ref{sec:regionfile} for a more detailed explanation)
	\item[RECALCJACOBIAN] - full recalculation of the Jacobian matrix. Due to improvement in computation speed this should stay on maybe except large 3D data 
	\item[ZWEIGHT] - new and more stable scheme for anisotropic constraints (replacing ZPOWER):
		\begin{equation}\label{eq:}
			w_c = 1 + n_z ( w_z - 1)
		\end{equation}
	\item[CHI1OPT] - $\chi^2(\lambda)=1$ optimization instead of fixed $\lambda$ or L-curve
	\item[LAMBDADECREASE] - decrease $\lambda$ in every inversion step
	\item[LOCALREGULARIZATION] - constrain only model update (not model itself)
%	\item[ISNOTREFERENCE] - (!) ????was wollte ich damit?
	\item[RESOLUTIONFILE] - file of indices or positions to compute resolution matrix columns
\end{description}

\subsubsection{Timelapse inversion}\label{sec:newtl}
\begin{description}
	\item[LAMBDATIMELAPSE] - defines regularization strength for time step inversion
	\item[TIMELAPSECONSTRAINT] - use different constraint type for time-lapse
	\item[FASTTIMELAPSE] - does not perform Jacobian update in time steps
	\item[TIMELAPSEREGIONFILE] - apply region file for time step inversion
	\item[TIMELAPSEREMOVEMISFIT] - Remove misfit from t=0 (LaBrecques method)
	\item[RATIOSTEP] - only one ratio step as in BERT1
\end{description}
See section \ref{sec:timelapse} on more details about timelapse inversion strategies.

\subsubsection{Mesh and forward calculation options}\label{sec:newmesh}
\begin{description}
	\item[PARAMESH] - use ready mesh or mesh generating script\footnote{Note that in case of topography additionally a mesh poly file must be provided as a basis for the primary mesh.}
	\item[PARAEDGELENGTH] - overrides PARAMAXCELLSIZE by calculating the latter using formulas for regular triangles/tetrahedra	
	\item[REGULARDZ] - creates a regular discretization using scalar or vector (!)
	\item[TOTALPOTENTIAL] - prevents singularity removal
	\item[P2CALCULATION] - use quadratic refined forward mesh (interesting in combination with TOTALPOTENTIAL) (!)
	\item[OMITSECMESHREFINE] - uses parameter mesh also for forward calculation
	\item[SECMESH] - uses externally created secondary mesh
\end{description}

Additionally to the script polyFlatWorld there is a script called polyFreeWorld, which is very similar but does not include the electrodes as nodes (and does no refinement).
Due to the latter you should use a small PARAMAXCELLSIZE to obtain (globally) fine enough meshes.

\subsubsection{Plotting options}\label{sec:newplot}
For using pytripatch, several options can be defined
\begin{description}
	\item[PYTRIPATCH] - location of pytripatch.py replacing paraview for 2d mesh plots
	\item[cMin,cMax] - minimum/maximum color scale used for pytripatch plots
	\item[INTERPERC] - outer interpercentile for automatic color scale (3 means use 3\% and 97\% of the cumulative histogram for the color scale ends)
	\item[USECOVERAGE] - specifies whether coverage is used for alpha shading
	\item[SHOWELECTRODES] - show electrodes with black points
\end{description}

\subsection{Deprecated options}
There are some variables that are not needed any more due to improved automatics.
\begin{description}
	\item[SPACECONFIG] - the default geometric factor is now automatic
	\item[INPUTOHM] - input content must now be conform to unified data file definition
	\item[FILTERVALUES] - was only experimental and is replaced by {\bf *MAX/MIN} switches
	\item[RHOSTART] - done by {\bf REGIONFILE}
	\item[LINSOLVER] - solver for linear system chosen automatically
	\item[ZPOWER] - anisotropic constraints are controlled by {\bf ZWEIGHT} or {\bf REGIONFILE}
	\item[NOPROLONGATION] - prolongation can be controlled via region file
\end{description}

Still not clear:\\
ELECTRODENODES
CYLINDER
CIRCLE
DIPOLE
SINGVALUE
ICDROPTOL

To check or to implement:\\
dcedit -T (trim mean), -U (del unused electrodes) and -A (force absolute rhoa)?\\

There are different constraint types that can be set using the CONSTRAINT key or in the region file. See section \ref{sec:regionfile} for details.

\subsection{Files and filenames}\label{sec:files}
As before, the meshes are saved in the directory mesh.
The sensitivity directory sensM is of very rare use but could still be used (BERT1 compatibility).

The directory primaryPot still holds the primary potentials.
Only for reasons of backward-compatibility there can be the sub-folders primpot and interpolated.
By default, potentials are now saved directly in the primaryPot folder in a single file pot.bmat (3D) or pot\_s.bmat (for potentials in wave-number domain required for 2d problems).

The log file for the inversion itself is now called bert.log.
The user bert options file in the home directory is still .bertrc.
Additionally there might be .bertrc file for all users where bertopts is located.

\subsection{The region file}\label{sec:regionfile}
The main control over the inversion is done by a so-called region file using the keyword \verb|REGIONFILE| (and \verb|TIMELAPSEREGIONFILE| for the time step inversion).
Of course, many options such as \verb|LAMBDA|, \verb|ZWEIGHT|, \verb|UPPERBOUND|, \verb|LOWERBOUND| can be set directly in the cfg file for the whole inversion domain (whether it is one region or more), but can be adjusted for different regions individually.
For a more rigorous description we refer to the GIMLi tutorial, appendix C.
The region file is a column file with different parts separated by a token list (led by a \# character), which can look like
\begin{verbatim}
#no start Ctype MC  zWeight Trans lBound uBound
0   100   1     1   0.2     log   50     1000
1   30    0     0.2 1       log   10     200
#no single start
2   1      100
#no background
-1  1
#inter-region
0   2  0.2
\end{verbatim}

The number No can also be a * for all regions and should lead the token list.
The other tokens define values for the numbered token.
\begin{description}
	\item[start] starting resistivity
	\item[Ctype] constraint type (1/-smoothness 1st/2nd order, 0-minimum length, 10/20 mixed 1st/2nd with zeroth order)
	\item[MC] model control (individual lambda relative to the global lambda)
	\item[zWeight] constraint weight for vertical boundaries (only for Ctype=1)
	\item[trans] model transformation, log(LU) by default, others are lin or tan
	\item[lBound] lower resistivity bound (for Trans=log)
	\item[uBound] upper resistivity bound (for Trans=log)
	\item[single] this region is treated such that the resistivity is constant
	\item[background] this region is background and filled with resistivity of the neighbouring regions for the forward calculation, for a non-Neumann domain without region file the lowest number is the background by default
	\item[inter-region] two regions are by default decoupled (constraint weight is zero), but can be coupled
\end{description}

In the above example there are two main regions with different starting values, valid ranges and constraints.
Another region is treated constant and is coupled to region 0 weakly.
Finally, there is one background region.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\section{Static examples using different meshes and region files}
Of course, all old BERT 1.0 examples can be computed by BERT 2 using the same cfg files.
Some of them can of course be improved by additional control.

\subsection{Region concept using the underwater example}
The files are located in the directory \verb|examples/inversion/lake|.
In section 2.6 of the BERT 1 tutorial we presented a sophisticated example with a lake of known (or unknown) resistivity as dedicated region.
The lake itself was given the fixed marker of the background and thus treated as such. 
In order to treat it as fixed resistivity in the forward calculation, we set the starting resistivity to 22.5\,$\Omega$m and prohibited the default resistivity prolongation from the inversion region into the background (such that the whole background obtains 22.5\,$\Omega$m).
This is ok for the lake but wrong for the outside and probably causes the resistivity increase towards the boundary that has to compensate the effect of the conducting outer space.

In Figure \ref{fig:lake}a the inversion result using normal treatment i.e. smoothness constraints across the whole model is shown.
We see a lot of structures related to the lake bottom and unrealistic high water resistivity.
In subfigure b the result is shown that arises by pure decoupling of the two regions, as already the poly script defines\footnote{The first subfigure can either be reached as for cross-hole data (polyFlatWorld) or by inter-region constraints in the region file.}
At already distinguishes the lake from the subsurface but yields a too low water resistivity (we measured 22-23\,$\Omega$m at several positions) and additional artifacts in the lake.

Actually the lake is a different region in the subsurface that can be treated individually.
A so-called region file controls the behaviour of the regions and is introduced into the cfg file by
\begin{verbatim}
REGIONFILE=region.control
\end{verbatim}

We first define the water region (marker 3) as a single-parameter region and the subsurface as a normal inversion region with smoothness constraints that enhance vertical structures, a range of 10-1000\,$\Omega$m and a starting resistivity of 100\,$\Omega$m:
\begin{verbatim}
#No	start	Trans	zWeight	Ctype	lBound	uBound
2	  100   log	  0.1	    1	    10	    1000
#No	single start
3	  1	    22.5
#No	background
1	1
\end{verbatim}

\begin{figure}[p]%
\centering
\includegraphics[width=\textwidth]{lake-normal}\\
\includegraphics[width=\textwidth]{lake-decoupled}\\
\includegraphics[width=\textwidth]{lake-single}\\
\includegraphics[width=\textwidth]{lake-fixed}\\
\includegraphics[width=\textwidth]{lake-interregion}\\
\includegraphics[width=\textwidth]{lake-couple}\\
\includegraphics[width=0.8\textwidth]{lake-cbar}\\
\caption{Inversion result with different options (from top to bottom): a) normal inversion, b) only decoupling, c) lake as a single-parameter region, d) lake as a fixed single-parameter region, e) inter-region constraints between lake and lake-bottom, f) as e but variable water layer}%
\label{fig:lake}%
\end{figure}

Figure \ref{fig:lake}c shows the inversion result.
The main image is similar but the oscillation at the sea-bottom are gone.
The resistivity increase at the bottom disappeared and reveals more information about the deeper layers.
However, the resistivity of the lake obtains values of about 16\,$\Omega$m, which is lower than the expected value of 22.5\,$\Omega$m measured at the side, even though we used it as starting value.

In order to narrow the resistivity value of the lake we expand the lines for region 3:
\begin{verbatim}
#No	single start Trans lBound uBound
3	  1	     22.5	   log   22     23
\end{verbatim}

The result, shown in Figure~\ref{fig:lake}c, is equally well fitting the data.
Both results are obviously equivalent, in this full-space problem the lake resistivity cannot be independently obtained and has to be added by additional information.

Alternatively we can assume that the lake bottom sediments have very similar resistivity to the water.
Therefore we remove the range constraint again and introduce inter-region constraints between the two regions:
\begin{verbatim}
#No	single start
3   1      22.5
#Inter-region
2   3      0.5
\end{verbatim}

So the otherwise decoupled regions are connected by smoothness constraints of a factor 2 weaker than normal.
The result, equivalently fitting the data, is shown in Figure \ref{fig:lake}d.
It is very similar to Fig.~\ref{fig:lake}c and thus supporting the measured conductivity.

On the other hand it is not clear that the water resistivity is constant.
So we can treat the water as normal region with proper upper and lower bounds.
In order to have our target structure in the water layer we increase the model control (relative regularization parameter) for that region by a factor of 2.
The inter-region constraints are kept.
\begin{verbatim}
#No	start	Trans	zWeight	Ctype	lBound	uBound MC
2   25    log   0.1     1     10      1000   1
3   22.5  log   0.01    1     10      30     3
#Inter-region
2   3     0.5
\end{verbatim}
The result (Fig.~\ref{fig:lake}e) however, is not very different from the other.


\subsection{Different mesh types}
We take the 2d cross-hole example from section 5.1 of the BERT 1 tutorial, a shallow 2d cross-hole ERT measurement with 8 boreholes \citep{kurasetal09}.
The files are located in the directory \verb|examples/inversion/2dxh|.
First we create a classic unstructured mesh using the electrodes as nodes using\\
\protect{\verb|PARAGEOMETRY="polyFlatWorld $DATAFILE"|}.

The script uses the parameter \verb|PARABOUNDARY| to construct a box around the electrodes.
Next we use the very similar script polyFreeWorld by specifying\\
\verb|PARAGEOMETRY="source polyFreeWorld $DATAFILE|.

Since there a only few restrictions, the resulting triangle mesh is very coarse over the whole area.
We could place some points to ensure locally small triangles and use a good mesh quality for slowly increasing edge lengths.
Alternatively we can (globally) provide a maximum triangle area, e.g. by using the new option \verb|PARAEDGELENGTH|, where it the area is computed based on a regular triangle.
By setting the actual value to 0.05 we obtain a fine enough mesh.

The third way is to create a regular mesh, either by using a script based on the HowTo in the BERT tutorial or by specifying (NOT READY YET! Have a look into the script itself) \verb|REGULAR=0.5| or \verb|REGULAR=regvec| (regvec.x and regvec.z holding complete vectors or min/max/diff).

Figure~\ref{fig:meshtypes} shows the three obtained results.
In general the resistivity distribution is very similar, i.e. the good conductor at depth and the resistor on the top left.
However there are differences in the small-scaled aquifer anomalies.
\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{2dxh-node}\\
\includegraphics[width=0.5\textwidth]{2dxh-free}\\
\includegraphics[width=0.5\textwidth]{2dxh-reg}
\caption{Inversion result for different mesh types: triangle mesh with electrodes nodes (top) or free electrodes (middle), rectangular mesh (bottom).}\label{fig:meshtypes}
\end{figure}

\section{Time-lapse inversion}\label{sec:timelapse}
\subsection{Strategies}
There is a large number of different strategies for doing time-lapse inversion.
\begin{enumerate}
	\item The easiest is an independent inversion, which can be easily done using a simple shell script that just overwrites \verb|DATAFILE|. However, very frequently artifacts arise, especially when looking at the changes (ratios).
	\item Another variant is ratio or quotient inversion, i.e. calculating the data ratio and inverting them as apparent resistivity \citep{SchuetzeFriedelJacobs2002}. However, this valid only for small contrasts since it does not take the sensitivity distribution as function of the real model into account.
	\item BERT1 used a simplified one-step scheme, where an inverse subproblem is solved for the apparent resistivity ratio in each time step (frame). However this is only valid for small changes and there is no information on the data fit. It is applied using \verb|RATIOSTEP=1|.
	\item There is the class of reference model based schemes, where for each frame a full minimization is done, but the models are constrained taking the model of either the first (default) or the preceding (\verb|TIMELAPSESTEPMODEL=1|) frame as reference. It is the most general scheme since the used measuring arrays and even the electrode positions can vary over time. Therefore this is the default method.
	\item A special scheme is the so-called difference inversion after \cite{labrecqueyang01}. It is based on the observation that there is a significant amount of systematic errors in all time steps. Therefore, if \verb|TIMELAPSEREMOVEMISFIT=1|, the data $d^k$ of $k^{th}$ frame is corrected by the misfit of the first frame ($\mathbf{d^0}$) such that
	\[ \| \mathbf{d^k} - \mathbf{f(m^k)} 
	    - \mathbf{d^0} + \mathbf{f(m^0)} \| \]
	is minimized next to the regularization of the model difference $\mathbf{m^k}-\mathbf{m^0}$. We recommend this in case of known systematic errors. If only systematic errors are present this will only increase error and thus smoothness. At any rate, identical arrays are used.
	\item From an inversion point of view, the most rigorous method is a full discretization in time (4D inversion). There are Python scripts doing this efficiently using block matrices but this is still not stable enough.
\end{enumerate}

\subsection{Processing and options}

Data for the different frames are in single files and specified using the key \verb|TIMESTEPS|.
If there are many files, a subdirectory holding the files is advisable.
An issue of ongoing research are time-depending error models.
Analysis of normal-reciprocal data can possibly overcome the problem of differing effective smoothness. 

Generally, the arrays can be different.
However, to use the difference inversion, they must be identical.
Therefore the data are preprocessed into a data cube (number vs. time).
Non-existing data or measurements outside the filter rules stay in the file, but are down-weighted by a large error.

To define the time-lapse strategy, the above mentioned keys are used.
By default, reference inversion against the first frame is done, except \verb|TIMELAPSESTEPMODEL=1| determines regularization of time steps.
La~Brecque's method (different inversion) is used if \verb|TIMELAPSEREMOVEMISFIT=1|.

Time-lapse inversion can take long computing times.
If the changes are small, the main computational effort, the re-calculation of the Jacobian matrix can be omitted by using 	\verb|FASTTIMELAPSE=1|, however only effective in case of identical arrays.
Alternatively, the one-step method of BERT1 is activated by \verb|RATIOSTEP=1|.

Since the model difference has generally different range and shape, its regularization can differ from the static inversion.
The key \verb|LAMBDATIMELAPSE| defines regularization strength, \verb|TIMELAPSECONSTRAINT| the type of constraints (0,1,2,10,20).
Additionally, region file \verb|TIMELAPSEREGIONFILE| can be applied, e.g. by restricting resistivity ranges or keeping the changes low or smooth in some region.

Whereas 0th order constraints (no smoothness) are often prohibitive for static inversion due to the large artifacts, it can be interesting for time-lapse problems, since smoothness does not systematically change the model space.

The results of the inversion are contained in the binary files diffAbs.bmat and diffRel.bmat.
Furthermore, for each frame a file dcinv.result\_i.vtk is created so that in Paraview they are automatically loaded as time steps.
The vtk files hold both absolute values and relative changes with respect to the first.
For 2D inversions, a multi-page pdf file of the absolute resistivities or their ratios is created with the target mkpdf.

\subsection{Example}
We take the 2d crosshole example from section \ref{fig:meshtypes}.
A saline tracer is injected into the 8th borehole and monitored every 40\,min \citep{kurasetal09}.
We unpack the zip file and un-comment \verb|TIMESTEPS=timesteps.txt|.
The files have been preprocessed in a way that the measuring arrays are preserved, but apparent resistivities below 10 and above 200\,$\Omega$ are discarded by associating a large error.
We decide for any parameterization strategy, e.g. the regular grid.


\bibliographystyle{seg}
\bibliography{bert}

\end{document}
