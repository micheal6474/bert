#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os

try:
    import numpy as np
    print "succsesfully loaded numpy"
except:
    sys.stderr.write('''ERROR: cannot import the library numpy. Ensure that numpy is installed. If not get one copy from: http://sourceforge.net/projects/numpy/files/''')

try:
    import matplotlib as mpl
    print "succsesfully loaded matplotlib"
except:
    sys.stderr.write('''ERROR: cannot import the matplotlib. Ensure that matplotlib is installed. If not get one copy from: http://matplotlib.sourceforge.net/''')

try:
    print "checking for dcfemlib apps: "
    os.system( 'meshconvert -V' )
    print "... ok"    
except:
    sys.stderr.write('''ERROR: cannot start bert 1 applications. Ensure that bert4win is installed and in your path. see INSTALL''')

try:
    print "checking for bert2 apps: "
    os.system( 'dcinv --version i' )
    print "... ok"    
except:
    sys.stderr.write('''ERROR: cannot start bert 2 applications. Ensure that bert2 is installed and in your path. see INSTALL''')

try:
    import pygimli as pg
    print "succsesfully loaded pygimli", g.version()
except:
    sys.stderr.write('''ERROR: cannot import the library 'pygimli'. Ensure that pygimli is in your PYTHONPATH ''')

try:
    print "checking for pytools: "
    os.system( 'pytripatch --version' )
    print "... ok"    
except:
    sys.stderr.write('''ERROR: cannot start bert 2 applications. Ensure that bert2 is installed and in your path. see INSTALL''')