# Boundless Electrical Resistivity Tomography (BERT)

Build status: [![Build Status](http://www.pygimli.org/build_status_bert.svg)](http://www.pygimli.org/build_bert.html)

You may take a look at our [Tutorial](http://www.resistivity.net/download/bert-tutorial.pdf) first.

www.resistivity.net

www.pygimli.org

## Installation [![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/installer/conda.svg)](https://conda.anaconda.org/gimli) [![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/downloads.svg)](https://anaconda.org/gimli/pybert)

On Linux platforms, the most comfortable way to install bert is via the conda
package manager contained in the [Anaconda
distribution](https://docs.continuum.io/anaconda/install#linux-install).
Anaconda is a scientific Python distribution with more than 100 Python packages
included (~400 Mb). You can also use the lightweight alternative
[Miniconda](http://conda.pydata.org/miniconda.html) (~35 Mb) and only install
the packages you like to use. Notes on how to install Miniconda (without root
privileges) can be found
[here](https://gitlab.com/resistivity-net/bert/wikis/install-miniconda).
Anaconda/Miniconda is available for both major Python versions and pybert will
work with both of them. However, we strongly recommend to use Python 3 and will
drop support for Python 2 eventually.

```bash
# Add gimli and conda-forge channel (you only need to do this once)
conda config --add channels gimli --add channels conda-forge

# Install pybert (and all dependencies such as pygimli, numpy, mpl, tetgen)
conda install -f pybert

# After installation, you can try out the examples for testing
cd ~/miniconda3/share/examples/inversion/2dflat/gallery
bert gallery.cfg all
bert gallery.cfg show

# Update to a newer version
conda update -f pygimli pybert
```

# Windows binary installers

On Windows 64bit platforms (Win 7, 8 and 10) we provide binary installers named py36, py35 (and py34) to be used along with a 64bit Python 3.6.x, 3.5.x (or 3.4.x), respectively.
We recommend  package installers like Anaconda or WinPython that bring along a lot of useful packages like numpy and matplotlib (used for numerics and plotting).

There are some additional hints in Appendix A of [Tutorial](http://www.resistivity.net/download/bert-tutorial.pdf). 
The newest Windows installers are found here:

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.6RC-green.svg)](http://www.resistivity.net/download/setup-bert2.2.6win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.5%20bert2.2.6RC-green.svg)](http://www.resistivity.net/download/setup-bert2.2.6win64py35.exe)

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.5-green.svg)](http://www.resistivity.net/download/setup-bert2.2.5win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.5%20bert2.2.5-green.svg)](http://www.resistivity.net/download/setup-bert2.2.5win64py35.exe)

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.4-green.svg)](http://www.resistivity.net/download/setup-bert2.2.4win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.5%20bert2.2.4-green.svg)](http://www.resistivity.net/download/setup-bert2.2.4win64py35.exe)

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.3-green.svg)](http://www.resistivity.net/download/setup-bert2.2.3win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.5%20bert2.2.3-green.svg)](http://www.resistivity.net/download/setup-bert2.2.3win64py35.exe)

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.2-green.svg)](http://www.resistivity.net/download/setup-bert2.2.2win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.5%20bert2.2.2-green.svg)](http://www.resistivity.net/download/setup-bert2.2.2win64py35.exe)

[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.1-green.svg)](http://www.resistivity.net/download/setup-bert2.2.1win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.5%20bert2.2.1-green.svg)](http://www.resistivity.net/download/setup-bert2.2.1win64py35.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.4%20bert2.1.1-green.svg)](http://www.resistivity.net/download/setup-bert2.1.1win64py34.exe)

We avoid installers for 32bit Windows (very rare). There might be installers for Python 2.7 (64bit) upon request, however we will drop Python 2.7 compatibility eventually.
Please ask the developers directly for (older) versions on 32bit or Py27.

Please direct new users to this project page instead of just sharing this link so that they will get aware for new versions. 
Please note that they will (due to a bug in gitlab) need a gitlab account that needs to be activated, so send your account data by email.

## Compiling from source

You need a working [GIMLi installation](http://www.pygimli.org/installation.html) first.
Then create your BERT target path preferable at the same level as your GIMLi path.

```bash
mkdir bert
cd bert
```

Clone your local copy of BERT using your GitLab account.

```bash
git clone https://gitlab.com/resistivity-net/bert.git
```

Create a build path.

```bash
mkdir build
cd build
```

Configure your build.

```bash
cmake ../bert
```

The -G option can be used to specify cmake options, e.g. for Windows

```bash
cmake -G "MSYS Makefiles"
```

You might have to specify the directories where GIMLI they are not found automatically.

```bash
cmake ../bert -DGIMLI_SRC=../../gimli/gimli
```

Or you can specify the path to the library and the header files separately.

```bash
cmake ../bert -DGIMLI_LIBRARIES=/path/to/libgimli.so -DGIMLI_INCLUDE_DIR=/path/to/gimli/src
```

After cmake finds everything, compile the BERT applications (dcmod, dcinv and dcedit).

```bash
make
```

Note that there is no core C++ library anymore since June 2017 when the core moved to pygimli.
If you need the polyTools and mesh generation stuff (recommended) run additionally:

```bash
make bert1
```

Until we fixed some issues in the automatic installation, you can simple add
the bert paths user setting variables.

```bash
export PATH=$PATH:PATH_TO_YOUR_BERTROOT/build/bin
export PYTHONPATH=$PYTHONPATH:PATH_TO_YOUR_BERTROOT/bert/python
```

Some further hints, troubleshooting or additional cmake commands are the same
as for your GIMLi installation and can be found here
<http://pygimli.org/installation.html>.

## Update your installation

To update your installation go into your BERT source repository.

```bash
cd bert/bert
git pull
```

Then you need to rebuild BERT.

    cd ../build
    make
    make bert1

If something goes wrong try to clean your build directory and repeat from the
cmake command above.
