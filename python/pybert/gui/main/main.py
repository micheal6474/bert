#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
#import wx
#import wx.xrc as xrc

import time

try:
    import pygimli as g
except ImportError as e:
    print("Here:", __file__)
    sys.stderr.write("Error: cannot import the library 'pygimli'. "
                     "Ensure that pygimli is in your PYTHONPATH \n")
    print(e) 
    sys.exit(1)

from pygimli.gui.main import QtGui, QtCore
from pygimli.gui.main import PyGUISystemMainFrame

#from pygimli.gui.resources import loadIcon

from . bertApp import BertApp

class PyBERTMainFrame(PyGUISystemMainFrame):
    def __init__(self, ws):
        super().__init__(ws)
        g.savePythonGIL(True)
        
        #self.setTitle('pyBERT')
        
        #mbFileMenu = self.MenuBar.GetMenu(self.MenuBar.FindMenu('File'))

        #item = wx.MenuItem(mbFileMenu, wx.NewId(), "&New\tCtrl-N", "Create new pyBERT project")
        #item.SetBitmap(loadIcon('document-new-from-template-16.png'))
        #mbFileMenu.AppendItem(item)
        #self.Bind(wx.EVT_MENU, self.onNew, id = item.GetId())
        
        #tbopen = self.toolBar_.AddSimpleTool(wx.NewId(), "Open", loadIcon('document-open-16.png')
                                            #, "Open project")
        #wx.EVT_TOOL(self, tbopen.GetId(), self.onOpenFileDialog)
        
        #tbnew = self.toolBar_.AddSimpleTool(wx.NewId(), "New", loadIcon('document-new-from-template-16.png')
                                            #, "New")
        #wx.EVT_TOOL(self, tbnew.GetId(), self.onNew)
        
        #self.toolBar_.Realize()

    def onAbout(self, event):
        '''
            Overwrite default about widget
        '''
        from wx.lib.wordwrap import wordwrap
        info = wx.AboutDialogInfo()
        info.Name = "pyBERT"
        info.Version = "0.8.0"
        info.Copyright = str("(C) 2011 Carsten Rücker and Thomas Günther", 'utf8')
        info.Description = wordwrap(
            "A GUI for BERT based on pyGIMLi: " +
            "wyPython-" + wx.VERSION_STRING +  " , ".join(wx.PlatformInfo[1:]) + ", \n" +
            " Running on python-" + sys.version.split()[0]
            , 350, wx.ClientDC(self))
            
        info.WebSite = ("http://www.resistivity.net")
        info.Developers = [ str("Carsten Rücker (carsten@resistivity.net)", 'utf8'),
                            str("Thomas Günther (thomas@resistivity.net)", 'utf8') ]
        info.License = wordwrap("licenseText", 500, wx.ClientDC(self))

        # Then we call wx.AboutBox giving it that info object
        wx.AboutBox(info)
        #-1-1 self.aboutGIMLiLabel.SetLabel(g.version())
        #self.aboutGIMLiDialog.Show()
        
    def onNew(self, event):
        '''
            New button menu entry or button is pressed
        '''
        bertApp = BertApp(self, self.rendererSlot, self.propertyInspectorSlot)
        
        self.resourceTree.addItem(bertApp, "BERT")
        self.resourceTree.SelectItem(bertApp.treeItem)

    def onOpenFileDialog(self, event):
        '''
        '''
        wildcard = "pyBERT project (*.pbp)|*.pbp|"     \
                   "All files (*.*)|*.*"

        dlg = wx.FileDialog(self, message="Choose a file",
                            defaultDir=os.getcwd(),
                            defaultFile="",
                            wildcard=wildcard,
                            style=wx.OPEN | wx.CHANGE_DIR | wx.FD_MULTIPLE)

        # Show the dialog and retrieve the user response. If it is the OK response,
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            # This returns a Python list of files that were selected.
            paths = dlg.GetPaths()
            print(('You selected %d files:' % len(paths)))

            for path in paths:
                self.openFile(path)

    # Compare this with the debug above; did we change working dirs?
        print(("CWD: %s\n" % os.getcwd()))

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def openFile(self, path):
        bertApp = BertApp(self, self.rendererSlot, self.propertyInspectorSlot)
        self.resourceTree.addItem(bertApp, "BERT")
        self.resourceTree.SelectItem(bertApp.treeItem)
        
        bertApp.openFile(path)


class PyBERTApp(QtGui.QApplication):
    def __init__(self, options=None, args=None, ws=None):
        super().__init__(sys.argv)
            
        import argparse
        parser = argparse.ArgumentParser(description="GUI for Bert")
    
        parser.add_argument("--debug", dest="debug", action="store_true",
                            help="Debug mode.", default=False)
        args = parser.parse_args()

        self.args = args
        self.logFile = 'pybert.log'
        
        self.mainFrame = PyBERTMainFrame(ws=ws)
        
        if not self.args.debug:
            self.mainFrame.redirectOutput(self.logFile)

        self.mainFrame.show()
        self.mainFrame.resize(800, 600)

    def start(self):
        """
        """
        sys.exit(self.exec_())
        

