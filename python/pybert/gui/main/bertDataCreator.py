# -*- coding: utf-8 -*-

import wx

import pygimli as g

from pygimli.gui.wxmpl import AppResourceWxMPL
from pygimli.mplviewer import *
from pybert.dataview import *

from .bertDataHandler import BertDataHandler

import numpy as np
from matplotlib.collections import PatchCollection

class BertDataCreator( BertDataHandler ):
    ''
    ' Create a new data set. '
    ''
    def __init__( self, parent, rendererSlot, propertyInspectorSlot ):
        BertDataHandler.__init__( self, parent, rendererSlot, propertyInspectorSlot )
        
        self.setName( "new data" )
        self.piName_            = "Data creator"
        self.schemeManager       = DataSchemeManager()
        
        self.initProperties()

    def initProperties( self ):
        ''
        ' Initialize all the properties we need '
        ''
        self.electrodeCount = self.appendProperty( "Electrode count", default = 24 , valType = int )
        self.electrodeSpacing = self.appendProperty( "Electrode spacing", default = 1.0, valType = float )
        self.electrodeOffset = self.appendProperty( "Electrode offset", default = 0.0, valType = float )
        self.electrodeMaxSeparation = self.appendProperty( "Electrode max Separation", default = 0, valType = int )
        self.electrodeConfigProp = self.appendProperty( "Electrode config", default = "Dipole Dipole", valType = str )
        self.electrodeInverseProp = self.appendProperty( "Electrode config inverse", default = False, valType = bool )

    def createPropertyPanel( self, parent ):
        """ Fill propery inspector (needed from base class) """
        panel = self.createPropertyInspectorNoteBookPanel( parent, 'piBertDataCreator', title = self.piName_ )

        self.electrodeCount.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'Bert/DataCreator/ElectrodeNumbers/TextCtrl' ) # name of the control in xrc
                                        , ctrlEvent = wx.EVT_KILL_FOCUS # the event that should observed
                                        , targetFunct = self.createData_ )                  # the callback when the event is called
                                        
        self.electrodeSpacing.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'Bert/DataCreator/ElectrodeSpacing/TextCtrl' ) # name of the control in xrc
                                        , ctrlEvent = wx.EVT_KILL_FOCUS # the event that should observed
                                        , targetFunct = self.createData_ )                  # the callback when the event is called
                                        
        self.electrodeOffset.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'Bert/DataCreator/ElectrodeOffset/TextCtrl' ) # name of the control in xrc
                                        , ctrlEvent = wx.EVT_KILL_FOCUS # the event that should observed
                                        , targetFunct = self.createData_ )                  # the callback when the event is called
        self.electrodeMaxSeparation.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'Bert/DataCreator/ElectrodeMaxSeparation/TextCtrl' ) # name of the control in xrc
                                        , ctrlEvent = wx.EVT_KILL_FOCUS # the event that should observed
                                        , targetFunct = self.createData_ )                  # the callback when the event is called
                                        
        self.electrodeConfigProp.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'Bert/DataCreator/ElectrodeConfig/Choice' ) # name of the control in xrc
                                        , ctrlEvent = wx.EVT_CHOICE                      # the event that should observed
                                        , targetFunct = self.createData_ )                  # the callback when the event is called

        self.electrodeInverseProp.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'Bert/DataCreator/Inverse/CheckBox' ) 
                                        , ctrlEvent = wx.EVT_CHECKBOX
                                        , targetFunct = self.createData_ )               
        
        shms = self.schemeManager.schemes()
        shms.remove('unknown')
        self.electrodeConfigProp.ctrl.AppendItems( shms )
                
        self.electrodeConfigProp.setVal( "Dipole Dipole (CC-PP)" )
        self.createData_( )

        # set default toolbar setting
        # auto aspect is a good idea here
        self.getToolBar().setAspectAuto( True )

        return panel
    # def createPropertyPanel( ... )

    def createData_( self ):
        ''
        ' A new configuration is selected '
        ''
        scheme = self.schemeManager.scheme( self.electrodeConfigProp() )
        print(scheme)
        scheme.setInverse( self.electrodeInverseProp() )
        scheme.setMaxSeparation( self.electrodeMaxSeparation() )
        self.data = scheme.create( self.electrodeCount(), self.electrodeSpacing() )
        self.data.translate( g.RVector3( self.electrodeOffset(), 0.0, 0.0 ) )
        self.draw()

    def drawData_( self ):
        ''
        ' Define what we have to be drawn (needed from base class) is called while a draw event is fired '
        ''
        self.electrodeMarker, self.dataMarker = drawDataAsMarker( self.axes, self.data, schemetype = self.schemeManager.scheme( self.electrodeConfigProp() ).typ )

    #END drawData( ... )
    
    def onExport( self, event = None ):
        """ Open file dialog to export the current active data """
        filename = self.getExportFileNameWithDialog( "data.shm", "GIMLi unified data format (*.shm)|*.shm|All files (*.*)|*.*" )
        
        if filename:
            self.data.save( filename, 'a b m n' )
    
    
    
    
    
    
    