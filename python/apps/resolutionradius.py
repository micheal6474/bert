import numpy as np
import scipy.linalg as lin
from scipy.sparse import coo_matrix, spdiags
import pygimli as pg
from math import pi
###############################################################################


def loadsens(sensname):
    """
       Load sensitivity matrix from BERT binary file.
    """
    fid = open(sensname, 'rb')
    ndata = int(np.fromfile(fid, 'int32', 1)[0])
    nmodel = int(np.fromfile(fid, 'int32', 1)[0])
    S = np.empty((ndata, nmodel), 'float')

    for i in range(ndata):
        S[i, :] = np.fromfile(fid, 'float', nmodel)

    print sensname + " loaded."
    print "Number of ABMNs: %s" % ndata
    print "Number of cells: %s" % nmodel

    return S


def load_constraint_matrix(fname, myshape=None):
    """ Load constraint matrix from BERT in sparse format """
    i, j, data = np.loadtxt(fname, unpack=True, dtype=int)
    if myshape is None:
        myshape = (max(i)+1, max(j)+1)
    C = coo_matrix((data, (i, j)), shape=myshape, dtype=int)

    return C


def compute_res(J, C, lam=1):
    """ Compute formal model resolution matrix and return its diagonal.

        Parameters:
        ===========
          J: Jacobian or sensitivity matrix
          C: Constraint (derivative) matrix
          alpha: tune amount of regularization
    """
    JTJ = J.T.dot(J)
    CTC = C.T.dot(C)  # C.T * C = (C_m)^-1

    RM = lin.inv(JTJ + CTC * lam).dot(JTJ)

    return RM
###############################################################################
if __name__ == "__main__":
    from optparse import OptionParser

    parser = OptionParser("usage: %prog [options] ",
                          version="%prog: " + pg.__version__)
    parser.add_option("-v", "--verbose", dest="verbose", action="store_true",
                      help="be verbose", default=False)
    parser.add_option("-D", "--logdata", dest="logdata", action="store_false",
                      help="linear data transform", default=True)
    parser.add_option("-M", "--logmodel", dest="logmodel", action="store_false",
                      help="linear model transform", default=True)
    parser.add_option("-l", "--lambda", dest="lam", type="float",
                      help="regularizations strength", default="10")
    parser.add_option("-p", "--paramesh", dest="meshfile", type="string",
                      help="parameter mesh", default="meshParaDomain.bms")
    parser.add_option("-s", "--sensmat", dest="sensfile", type="string",
                      help="sensitivity matrix", default="sens.bmat")
    parser.add_option("-c", "--cmatrix", dest="cmatfile", type="string",
                      help="constraint matrix", default="constraint.matrix")
    parser.add_option("-m", "--model", dest="modelfile", type="string",
                      help="model filename", default="resistivity.vector")
    parser.add_option("-r", "--response", dest="responsefile", type="string",
                      help="response vector", default="response.vector")
    parser.add_option("-w", "--cweight", dest="cweightfile", type="string",
                      help="constraint weight", default="")
    parser.add_option("-t", "--datatoken", dest="datatoken", type="string",
                      help="data token", default="rhoa")
    (options, args) = parser.parse_args()

    if options.verbose:
        __verbose__ = True

    if len(args) == 0:
        parser.print_help()
        print("Please add a data file name.")
        raise SystemExit
    else:
        datafile = args[0]

    # Load required files
    mesh = pg.Mesh(options.meshfile)
    J = loadsens(options.sensfile)
    data = pg.DataContainer(datafile)

    Ciso = load_constraint_matrix(options.cmatfile)
    if options.cweightfile:
        cw = np.genfromtxt(options.cweightfile)
    # Ciso = load_constraint_matrix(options.cmatfile, (len(cw), len(model)))
    else:
        cw = np.ones(Ciso.shape[0])

    C = spdiags(cw, 0, len(cw), len(cw)).dot(Ciso)

    # scale jacobian by data error
    for ii, ji in enumerate(J):
        ji /= data('err')[ii]

    # logarithmic model requires multiply by model
    if options.logmodel:
        model = np.genfromtxt(options.modelfile)
        mesh.addExportData('model', model)
        mesh.addExportData('model_log', pg.log10(model))
        for ii, ji in enumerate(J):
            ji *= model  # additionally divide by data error

    # Lgarithmic data requires divide by model response
    if options.logdata:
        if options.responsefile:
            response = np.genfromtxt(options.responsefile)
        else:
            response = data(options.datatoken)  # assume response is close

        for ii, ji in enumerate(J):
            ji /= response[ii]  # additionally divide by data error

    coverage = model * 0.
    for ji in J:
        coverage += pg.abs(ji)

    RM = compute_res(J, C, options.lam)  # full resolution matrix
    RR = np.maximum(np.diag(RM), 1e-4)  # prevent zero or negative resolution
    dim = mesh.dim() * 1.0  # shorter to use
    resrad = pg.pow(mesh.cellSizes() * dim / (dim * 2 - 2) / pi / RR, 1./dim)
    print('resolution radius min/max=', min(resrad), max(resrad))
    mesh.addExportData('resrad', resrad)
    mesh.addExportData('coverage_log', pg.log10(coverage))
    mesh.exportVTK('Mres_rad.vtk')
