Example 3 (borehole casing) from R�cker&G�nther (2011)

Current is injectd into a steel-cased borehole. A dipping plate is used as passive CEM body distorting current flow. Potential smeasurement are done by point electrodes at the surface. See the paper for details. 

calc.sh does the computation with CEM and with node electrodes.

TODO
    very old! needs to be updated to modern functions and styles.

References:
R�cker, C. & G�nther, T. (2011) The simulation of finite ERT electrodes using the complete electrode model. Geophysics, 76, F227.